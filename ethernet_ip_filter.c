#include "ethernet_ip_filter.h"

typedef unsigned int                u32;    /*!<u32*/
typedef unsigned short              u16;    /*!<u16*/
typedef short                       s16;    /*!<u16*/
typedef unsigned char               u8;     /*!<u8*/
typedef enum
{
    FALSE = 0,      /*!<FALSE，假*/
    TRUE = !FALSE   /*!<TRUE，真*/
}bool;


#include <stdlib.h>
#include <string.h>

#define MyMallloc   malloc
#define MyFree      free
#define MyMemCpy    memcpy

#ifndef WIN32
// #define MySemStruct             struct semaphore
// #define MySemInit(sem_ptr)      init_MUTEX_LOCKED(sem_ptr)
// #define MySemWait(sem_ptr)      down(sem_ptr)
// #define MySemPost(sem_ptr)      up(sem_ptr)

#include<semaphore.h>
#define MySemStruct             sem_t
#define MySemInit(sem_ptr)      sem_init(sem_ptr, 0, 1)
#define MySemWait(sem_ptr)      sem_wait(sem_ptr)
#define MySemPost(sem_ptr)      sem_post(sem_ptr)
#else

#define MySemStruct         int
#define MySemInit(sem_ptr)
#define MySemWait(sem_ptr)
#define MySemPost(sem_ptr)

#endif // WIN32


#pragma pack(1)
typedef struct CommonPacketItemType_st
{
    u16 TypeID;
    u16 Len;
    u8* Data;
}CommonPacketItemType;

typedef struct CommonPacketType_st
{
    u16                   ItemCount;    /*包含项个数*/
    CommonPacketItemType* ItemArray;    /*项存放指针数组*/
}CommonPacketType;

typedef struct EncapsulationHeaderType_st
{
    u16 Command;
    u16 Length;
    u32 SessionHandle;
    u32 Status;
    u8  SenderContext[8];
    u8  Options[4];
}EncapsulationHeaderType;

typedef struct EncapsulatedDataType_st
{
    u16 ItemCount;    /*包含项个数*/
}EncapsulatedDataType;

typedef struct EthernetIPType_st
{
    EncapsulationHeaderType* EncapsulationHeader;
    EncapsulatedDataType*    EncapsulatedData;
    CommonPacketItemType*    ItemArray;    /*项存放指针数组*/
}EthernetIPType;

typedef struct
{
    u8 PathSize;
    u16 ClassID;
    u16 InstanceNr;
    u16 AttributNr;
} S_CIP_EPATH;

typedef struct
{
    u8 Service;
    S_CIP_EPATH RequestPath;
    u16 DataLength;
    u8 *Data;
} S_CIP_MR_Request;

typedef struct
{
    u32 ConnectionIdentifier;
    u32 SequenceNumber;
} S_Address_Data;

typedef struct
{
    u16 TypeID;
    u16 Length;
    S_Address_Data Data;
} S_Address_Item;

typedef struct
{
    u16 TypeID;
    u16 Length;
    u8 *Data;
} S_Data_Item;

typedef struct
{
    u16 TypeID;
    u16 Length;
    s16 nsin_family;
    u16 nsin_port;
    u32 nsin_addr;
    u8 nasin_zero[8];
} S_SockAddrInfo_Item;

typedef struct
{
    u16 ItemCount;
    S_Address_Item stAddr_Item;
    S_Data_Item stDataI_Item;
    S_SockAddrInfo_Item AddrInfo[2];
} S_CIP_CPF_Data;

#pragma pack()

/* definition of known encapsulation commands */
#define COMMAND_NOP                     0x0000
#define COMMAND_LISTSERVICES            0x0004
#define COMMAND_LISTIDENTITY            0x0063
#define COMMAND_LISTINTERFACES          0x0064
#define COMMAND_REGISTERSESSION         0x0065
#define COMMAND_UNREGISTERSESSION       0x0066
#define COMMAND_SENDRRDATA              0x006F
#define COMMAND_SENDUNITDATA            0x0070
#define COMMAND_STATUS_CHECK            0x0072
#define COMMAND_CANCEL                  0x0073

/* define Item ID numbers used for address and data items in CPF structures */
#define CIP_ITEM_ID_NULL                                0x0000  /* Null Address Item */
#define CIP_ITEM_ID_LISTIDENTITY_RESPONSE               0x000C
#define CIP_ITEM_ID_CONNECTIONBASED                     0x00A1  /* Connected Address Item */
#define CIP_ITEM_ID_CONNECTIONTRANSPORTPACKET           0x00B1  /* Connected Data Item */
#define CIP_ITEM_ID_UNCONNECTEDMESSAGE                  0x00B2  /* Unconnected Data Item */
#define CIP_ITEM_ID_LISTSERVICE_RESPONSE                0x0100
#define CIP_ITEM_ID_SOCKADDRINFO_O_TO_T                 0x8000  /* Sockaddr info item originator to target (data) */
#define CIP_ITEM_ID_SOCKADDRINFO_T_TO_O                 0x8001  /* Sockaddr info item target to originator (data) */
#define CIP_ITEM_ID_SEQUENCEDADDRESS                    0x8002  /* Sequenced Address item */

static u16 ltohs(u8** pa_buf);
static u32 ltohl(u8** pa_buf);
static bool GetCIPProtocolContent(u8* ip, u32 ip_len, u8** cip, u32* length);
static bool EtherNetIpAnalysis(u8* buf, u32 len);
static bool FilterMatch(u16 commond, u16 class_id, u16 instance_id, u16 attribute_id, bool attr_enable);
static u8 createMRRequeststructure(u8* pa_pnData, u16 pa_nLength, S_CIP_MR_Request* pa_pstMRReqdata, bool* atrr_enable);
static u8 createCPFstructure(u8* pa_Data, int pa_DataLength, S_CIP_CPF_Data* pa_CPF_data);

static EthernetIPFilterType* EthernetIPFilterList = NULL;
static int EthernetIPFilterNumber = 0;

static MySemStruct FilterListSem;

/***************************************************************************************************
*\Function      EthernetIPFilterInit
*\Description   EthernetIP协议过滤初始化
*\Parameter     filter_list 协议过滤规则列表，类型为EthernetIPFilterCommandType类型的数组
*\Parameter     num         协议过滤规则数目
*\Return        int         是否初始化成功，0：成功，1：失败
*\Note
*               创建函数。
***************************************************************************************************/
int EthernetIPFilterInit(EthernetIPFilterType * filter_list, int num)
{
    static bool once_run = TRUE;

    if (once_run == TRUE)
    {
        once_run = FALSE;
        MySemInit(&FilterListSem);
    }

    /*安检*/
    if (filter_list == NULL || num == 0)
    {
        return 1;
    }

    /***************************************************************************************************
    *互斥
    ***************************************************************************************************/
    MySemWait(&FilterListSem);

    /*释放旧的规则*/
    if ((EthernetIPFilterList != NULL) && (EthernetIPFilterNumber != 0))
    {
        MyFree(EthernetIPFilterList);
    }

    EthernetIPFilterList = (EthernetIPFilterType*)MyMallloc(sizeof(EthernetIPFilterType) * num);
    EthernetIPFilterNumber = num;

    MyMemCpy(EthernetIPFilterList, filter_list, sizeof(EthernetIPFilterType) * num);
    /***************************************************************************************************
    *互斥结束
    ***************************************************************************************************/
    MySemPost(&FilterListSem);

    return 0;
}

/***************************************************************************************************
*\Function      EthernetIPFilterCheck
*\Description   EthernetIP协议数据包过滤检查
*\Parameter     ip_packet_buf           IP层数据包
*\Parameter     ip_packet_buf_length    IP数据包长度
*\Return        int                     该数据包是否可通过,0:通过，1：不能通过
*\Note
***************************************************************************************************/
int EthernetIPFilterCheck(char* ip_packet_buf, int ip_packet_buf_length)
{
    u8* cip = NULL;
    u32 cip_len = 0;

    if (ip_packet_buf == NULL || ip_packet_buf_length == 0)
    {
        return 1;
    }

    if (GetCIPProtocolContent((u8*)ip_packet_buf, ip_packet_buf_length, &cip, &cip_len) == FALSE)
    {
        return 1;
    }

    if (EtherNetIpAnalysis(cip, cip_len) == FALSE)
    {
        return 1;
    }

    return 0;
}

/***************************************************************************************************
*\Function      GetCIPProtocolContent
*\Description   从IP包中获取cip协议内容
*\Parameter     ip
*\Parameter     ip_len
*\Parameter     cip
*\Parameter     length
*\Return        bool
*\Note
***************************************************************************************************/
static bool GetCIPProtocolContent(u8* ip, u32 ip_len, u8** cip, u32* length)
{
    /*先去掉IP头*/
    u32 len = (ip[0] & 0x0f) * 4;   /*计算首部长度*/
    u8* tcpudp = (u8*)(ip + len);
    u8 protocol = ip[9];
    len = (ip[2])*256 + ip[3] - len;

    /*再去掉TCP头或UDP头*/
    if (protocol == 6)
    {
        *cip = tcpudp + (tcpudp[12]>>4) * 4;
        *length = len - (tcpudp[12]>>4) * 4;
    }
    else if (protocol == 17)
    {
        *cip = tcpudp + 8;
        *length = (tcpudp[4]*256+tcpudp[5] - 8);
    }
    else
    {
        return FALSE;
    }

    return TRUE;
}

/***************************************************************************************************
*\Function      EtherNetIpAnalysis
*\Description   EtherNet/IP解析
*\Parameter     buf     待解析数据
*\Parameter     len     待解析数据长度
*\Return        bool    数据分析结果
*\Note
***************************************************************************************************/
static bool EtherNetIpAnalysis(u8* buf, u32 len)
{
    static EthernetIPType adu;
    static S_CIP_CPF_Data CPFdata;
    static S_CIP_MR_Request MRdata;
    u16 i = 0;
    u8* data_ptr = NULL;
    u32 left_data_len = 0;
    bool attr_enable = FALSE;

    adu.EncapsulationHeader = (EncapsulationHeaderType*)buf;
    adu.EncapsulatedData    = (EncapsulatedDataType*)(buf + sizeof(EncapsulationHeaderType) + 6);


    if(len < sizeof(EncapsulationHeaderType))  /*报文长度太短*/
    {
        return FALSE;
    }
    /*报文正确性验证*/
    if(adu.EncapsulationHeader->Length > len - sizeof(EncapsulationHeaderType))
    {
        /*Header中标示数据长度 > 整帧长度 - sizeof(Command)*/
        /*数据帧不完整或解析错误*/
        return FALSE;
    }
    if(len - sizeof(EncapsulationHeaderType) < adu.EncapsulatedData->ItemCount * 4 + sizeof(adu.EncapsulatedData->ItemCount))
    {
        /*数据域长度 <（项个数*4 + sizeof(ItemCount))*/
        /*数据帧不完整或解析错误*/
        return FALSE;
    }

    

    /*如果不是发送消息的数据帧，则不进行资源的过滤*/
    if (adu.EncapsulationHeader->Command != COMMAND_SENDRRDATA && adu.EncapsulationHeader->Command != COMMAND_SENDUNITDATA)
    {
        /*过滤非发送消息的command是否可通过*/
        return FilterMatch(adu.EncapsulationHeader->Command, 0, 0, 0, FALSE);
    }

    if (adu.EncapsulatedData->ItemCount > 4)
    {
        /*组织各项数据*/
        data_ptr = (u8*)((u8*)&adu.EncapsulatedData->ItemCount + sizeof(adu.EncapsulatedData->ItemCount));                                               /*指向第一项数据的开头*/
        left_data_len = len - sizeof(EncapsulationHeaderType) - sizeof(EncapsulatedDataType) - 6; /*计算剩余数据长度*/
        adu.ItemArray = (CommonPacketItemType*)MyMallloc(sizeof(CommonPacketItemType) * adu.EncapsulatedData->ItemCount);

        /*循环组织各项数据*/
        for (i = 0; i < adu.EncapsulatedData->ItemCount; i++)
        {
            adu.ItemArray[i].TypeID = ((CommonPacketItemType*)data_ptr)->TypeID;  /*TYPE_ID*/
            adu.ItemArray[i].Len    = ((CommonPacketItemType*)data_ptr)->Len;     /*数据长度*/
            adu.ItemArray[i].Data   = data_ptr + 4;                               /*数据指针*/
            data_ptr += adu.ItemArray[i].Len + 4;                                 /*后移指针*/
            left_data_len -= (adu.ItemArray[i].Len + 4);
            if (adu.ItemArray[i].TypeID == CIP_ITEM_ID_CONNECTIONTRANSPORTPACKET || adu.ItemArray[i].TypeID == CIP_ITEM_ID_UNCONNECTEDMESSAGE)
            {
                /*判断数据请求路径*/
                if (createMRRequeststructure(adu.ItemArray[i].Data, adu.ItemArray[i].Len, &MRdata, &attr_enable) == 1)
                {
                    MyFree(adu.ItemArray);
                    return FALSE;
                }

                /*资源过滤*/
                if (FilterMatch(adu.EncapsulationHeader->Command, MRdata.RequestPath.ClassID, MRdata.RequestPath.InstanceNr, MRdata.RequestPath.AttributNr, attr_enable) == FALSE)
                {
                    MyFree(adu.ItemArray);
                    return FALSE;
                }
            }


            /*判断剩余数据长度是否大于剩余数据项的最小长度*/
            if(left_data_len < (u32)((adu.EncapsulatedData->ItemCount - i - 1) * 4))
            {
                MyFree(adu.ItemArray);
                return FALSE;
            }
        }

        /*数据项获取完毕*/
        MyFree(adu.ItemArray);
    }
    else
    {
        data_ptr = (u8*)adu.EncapsulatedData;
        left_data_len = len - sizeof(EncapsulationHeaderType) - 6;
        if (createCPFstructure(data_ptr, left_data_len, &CPFdata) == 1)
        {
            return FALSE;
        }

        if (CPFdata.stDataI_Item.TypeID == CIP_ITEM_ID_CONNECTIONTRANSPORTPACKET || CPFdata.stDataI_Item.TypeID == CIP_ITEM_ID_UNCONNECTEDMESSAGE)
        {
            /*判断数据请求路径*/
            if (createMRRequeststructure(CPFdata.stDataI_Item.Data, CPFdata.stDataI_Item.Length, &MRdata, &attr_enable) == 1)
            {
                return FALSE;
            }

            /*资源过滤*/
            if (FilterMatch(adu.EncapsulationHeader->Command, MRdata.RequestPath.ClassID, MRdata.RequestPath.InstanceNr, MRdata.RequestPath.AttributNr, attr_enable) == FALSE)
            {
                return FALSE;
            }
        }
    }

    return TRUE;
}

/***************************************************************************************************
*\Function      FilterMatch
*\Description   遍历过滤规则，看是否可以通过
*\Parameter     type    过滤类型
*\Parameter     value   要过滤的值
*\Return        bool    是否通过
*\Note
***************************************************************************************************/
static bool FilterMatch(u16 commond, u16 class_id, u16 instance_id, u16 attribute_id, bool attr_enable)
{
    int i = 0;

    if ((EthernetIPFilterNumber == 0) || (EthernetIPFilterList == NULL))
    {
        return FALSE;
    }

    /***************************************************************************************************
    *互斥
    ***************************************************************************************************/
    MySemWait(&FilterListSem);

    /*因为冲突时以最后设置的规则为准，因此从后往前遍历规则*/
    for (i = EthernetIPFilterNumber - 1; i >=0; i--)
    {
        /*判断commond是否合法*/
        if ((commond >= EthernetIPFilterList[i].Command_Start) && (commond <= EthernetIPFilterList[i].Command_End))
        {
            /*判断是否为消息发送命令，否则不进行资源过滤*/
            if ((commond == COMMAND_SENDRRDATA) || (commond == COMMAND_SENDUNITDATA))
            {
                /*资源过滤*/

                /*判断classid过滤是否启用*/
                if (EthernetIPFilterList[i].ClassIDEnable == 1)
                {
                    if ((class_id >= EthernetIPFilterList[i].ClassID_Start) && (class_id <= EthernetIPFilterList[i].ClassID_End))
                    {
                        /*判断instanceid过滤是否启用*/
                        if (EthernetIPFilterList[i].InstanseEnable == 1)
                        {
                            if ((instance_id >= EthernetIPFilterList[i].Instanse_Start) && (instance_id <= EthernetIPFilterList[i].Instanse_End))
                            {
                                /*判断attribute_id过滤是否启用,以及资源路径中是否指定了attribute*/
                                if ((EthernetIPFilterList[i].AtrributeEnable == 1) && (attr_enable == TRUE))
                                {
                                    if (!((attribute_id >= EthernetIPFilterList[i].Atrribute_Start) && (attribute_id <= EthernetIPFilterList[i].Atrribute_End)))
                                    {
                                        /*attribute_id不合法*/
                                        continue;
                                    }
                                }
                            }
                            else
                            {
                                /*instance_id不合法*/
                                continue;
                            }
                        }
                    }
                    else
                    {
                        /*classid不合法*/
                        continue;
                    }
                }                
            }

            MySemPost(&FilterListSem);
            return TRUE;
        }
    }

    /***************************************************************************************************
    *互斥结束
    ***************************************************************************************************/
    MySemPost(&FilterListSem);

    return FALSE;
}


/***************************************************************************************************
*\Function      createMRRequeststructure
*\Description   获取数据访问的路径，包括ClasssID、Instance、Atrrribute
*\Parameter     pa_pnData           数据域缓冲区指针
*\Parameter     pa_nLength          数据域缓冲区长度
*\Parameter     pa_pstMRReqdata     解析后的结构体
*\Parameter     atrr_enable         属性域是否有效
*\Return        u8                  是否解析成功，0：成功，1：失败
*\Note
***************************************************************************************************/
static u8 createMRRequeststructure(u8* pa_pnData, u16 pa_nLength, S_CIP_MR_Request* pa_pstMRReqdata, bool* atrr_enable)
{
    int i;

    pa_pstMRReqdata->Service = *pa_pnData;
    pa_pnData++;
    pa_pstMRReqdata->RequestPath.PathSize = *pa_pnData;
    pa_pnData++;

    /*获取classid、instanceid、attribut*/
    pa_pstMRReqdata->RequestPath.ClassID = 0;
    pa_pstMRReqdata->RequestPath.InstanceNr = 0;
    pa_pstMRReqdata->RequestPath.AttributNr = 0;
    *atrr_enable = FALSE;

    for (i = 0; i < pa_pstMRReqdata->RequestPath.PathSize; i++)
    {
        if (0xE0 == ((*pa_pnData) & 0xE0))
        {
            /*Invalid segment type*/
            return 1;
        }
        switch (*pa_pnData)
        {
        case 0x20: /* classID */
            pa_pstMRReqdata->RequestPath.ClassID = *(u8*) (pa_pnData + 1);
            pa_pnData += 2;
            break;

        case 0x21: /*classID 16Bit */
            pa_pnData += 2;
            pa_pstMRReqdata->RequestPath.ClassID = ltohs(&(pa_pnData));
            i++;
            break;

        case 0x24: /* InstanceNr */
            pa_pstMRReqdata->RequestPath.InstanceNr = *(u8*) (pa_pnData + 1);
            pa_pnData += 2;
            break;

        case 0x25: /* InstanceNr 16Bit */
            pa_pnData += 2;
            pa_pstMRReqdata->RequestPath.InstanceNr = ltohs(&(pa_pnData));
            i++;
            break;

        case 0x30: /* AttributeNr */
            pa_pstMRReqdata->RequestPath.AttributNr = *(u8*) (pa_pnData + 1);
            pa_pnData += 2;
            *atrr_enable = TRUE;
            break;

        case 0x31: /* AttributeNr 16Bit */
            pa_pnData += 2;
            pa_pstMRReqdata->RequestPath.AttributNr = ltohs(&(pa_pnData));
            i++;
            *atrr_enable = TRUE;
            break;

        default:
            return 1;
        }
    }

    pa_pstMRReqdata->Data = pa_pnData;
    pa_pstMRReqdata->DataLength = pa_nLength - ((pa_pstMRReqdata->RequestPath.PathSize) * 2 + 2);

    if (pa_pstMRReqdata->DataLength < 0)
    {
        return 1;
    }

    return 0;
}

/***************************************************************************************************
*\Function      createCPFstructure
*\Description   构造数据项列表结构体，从数据域中获取数据项
*\Parameter     pa_Data             CommPacket数据包的缓冲区地址
*\Parameter     pa_DataLength       CommPacket数据包的缓冲区长度
*\Parameter     pa_CPF_data         数据项列表存放的地址
*\Return        u8                  是否构造成功,0：成功，1:失败
*\Note          只有数据项个数为3或4的时候才能使用该函数(适合大多数情况)
***************************************************************************************************/
static u8 createCPFstructure(u8* pa_Data, int pa_DataLength, S_CIP_CPF_Data* pa_CPF_data)
{
    int len_count, i, j;

    pa_CPF_data->AddrInfo[0].TypeID = 0;
    pa_CPF_data->AddrInfo[1].TypeID = 0;

    /*获取数据项个数*/
    len_count = 0;
    pa_CPF_data->ItemCount = ltohs(&pa_Data);
    len_count += 2;
    if (pa_CPF_data->ItemCount >= 1)
    {
        /*第1项为地址信息*/
        pa_CPF_data->stAddr_Item.TypeID = ltohs(&pa_Data);
        pa_CPF_data->stAddr_Item.Length = ltohs(&pa_Data);
        len_count += 4;
        if (pa_CPF_data->stAddr_Item.Length >= 4)
        {
            pa_CPF_data->stAddr_Item.Data.ConnectionIdentifier = ltohl(&pa_Data);
            len_count += 4;
        }
        if (pa_CPF_data->stAddr_Item.Length == 8)
        {
            pa_CPF_data->stAddr_Item.Data.SequenceNumber = ltohl(&pa_Data);
            len_count += 4;
        }
    }
    if (pa_CPF_data->ItemCount >= 2)
    {
        /*第2项为数据信息*/
        pa_CPF_data->stDataI_Item.TypeID = ltohs(&pa_Data);
        pa_CPF_data->stDataI_Item.Length = ltohs(&pa_Data);
        pa_CPF_data->stDataI_Item.Data = pa_Data;
        pa_Data += pa_CPF_data->stDataI_Item.Length;
        len_count += (4 + pa_CPF_data->stDataI_Item.Length);
    }
    /*处理后面的sockaddress,过滤时用不着*/
    for (j = 0; j < (pa_CPF_data->ItemCount - 2); j++) 
    {
        pa_CPF_data->AddrInfo[j].TypeID = ltohs(&pa_Data);
        len_count += 2;
        if ((pa_CPF_data->AddrInfo[j].TypeID == CIP_ITEM_ID_SOCKADDRINFO_O_TO_T)
            || (pa_CPF_data->AddrInfo[j].TypeID == CIP_ITEM_ID_SOCKADDRINFO_T_TO_O))
        {
            pa_CPF_data->AddrInfo[j].Length = ltohs(&pa_Data);
            pa_CPF_data->AddrInfo[j].nsin_family = ltohs(&pa_Data);
            pa_CPF_data->AddrInfo[j].nsin_port = ltohs(&pa_Data);
            pa_CPF_data->AddrInfo[j].nsin_addr = ltohl(&pa_Data);
            for (i = 0; i < 8; i++)
            {
                pa_CPF_data->AddrInfo[j].nasin_zero[i] = *pa_Data;
                pa_Data++;
            }
            len_count += 18;
        }
        else
        { 
            pa_CPF_data->AddrInfo[j].TypeID = 0; 
            pa_Data -= 2;
        }
    }
    
    /*验证数据项的正确性*/
    if (pa_CPF_data->ItemCount < 4)
    {
        pa_CPF_data->AddrInfo[1].TypeID = 0;
        if (pa_CPF_data->ItemCount < 3)
        {
            pa_CPF_data->AddrInfo[0].TypeID = 0;
        }
    }
    if (len_count == pa_DataLength)
    { 
        return 0;
    }
    else
    {
        if (pa_CPF_data->ItemCount > 2)
        {
            
            return 0;
        }
        else
        { 
            return 1;
        }
    }
}

static u16 ltohs(u8** pa_buf)
{
    unsigned char *p = (unsigned char *) *pa_buf;
    u16 data = p[0] | p[1] << 8;
    *pa_buf += 2;
    return data;
}

static u32 ltohl(u8** pa_buf)
{
    unsigned char *p = (unsigned char *) *pa_buf;
    u32 data = p[0] | p[1] << 8 | p[2] << 16 | p[3] << 24;
    *pa_buf += 4;
    return data;
}
