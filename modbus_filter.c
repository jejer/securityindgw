#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <arpa/inet.h>

#include <libmnl/libmnl.h>
#include <linux/netfilter.h>
#include <linux/netfilter/nfnetlink.h>

#include <linux/types.h>
#include <linux/netfilter/nfnetlink_queue.h>

#include <libnetfilter_queue/libnetfilter_queue.h>


#include "modbus.h"
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/tcp.h>

#include "filter_config.h"


static unsigned char op_white_list[OP_WHITE_LIST_MAX] = {0};
static int op_cnt = 0;
static struct addr_control addr_control_list[ADDR_CONTROL_LIST_MAX];
static int addr_cnt = 0;
static int testmode = 0;
static int save_log = 0;
static struct audit_result mysql_result;
static int dev_id = 0;
static int bypass_log = 1;


static int modbus_filter(void* payload);

static struct mnl_socket *nl;

static struct nlmsghdr *
nfq_hdr_put(char *buf, int type, uint32_t queue_num)
{
    struct nlmsghdr *nlh = mnl_nlmsg_put_header(buf);
    nlh->nlmsg_type	= (NFNL_SUBSYS_QUEUE << 8) | type;
    nlh->nlmsg_flags = NLM_F_REQUEST;

    struct nfgenmsg *nfg = mnl_nlmsg_put_extra_header(nlh, sizeof(*nfg));
    nfg->nfgen_family = AF_UNSPEC;
    nfg->version = NFNETLINK_V0;
    nfg->res_id = htons(queue_num);

    return nlh;
}

static void
nfq_send_verdict(int queue_num, uint32_t id, int result)
{
    char buf[MNL_SOCKET_BUFFER_SIZE];
    struct nlmsghdr *nlh;

    nlh = nfq_hdr_put(buf, NFQNL_MSG_VERDICT, queue_num);
    nfq_nlmsg_verdict_put(nlh, id, result);

    if (mnl_socket_sendto(nl, nlh, nlh->nlmsg_len) < 0)
    {
        perror("mnl_socket_send");
        exit(EXIT_FAILURE);
    }
}

static int queue_cb(const struct nlmsghdr *nlh, void *data)
{
    struct nfqnl_msg_packet_hdr *ph = NULL;
    struct nlattr *attr[NFQA_MAX+1] = {};
    uint32_t id = 0, skbinfo;
    struct nfgenmsg *nfg;
    uint16_t plen;

    if (nfq_nlmsg_parse(nlh, attr) < 0)
    {
        perror("problems parsing");
        return MNL_CB_ERROR;
    }

    nfg = mnl_nlmsg_get_payload(nlh);

    if (attr[NFQA_PACKET_HDR] == NULL)
    {
        fputs("metaheader not set\n", stderr);
        return MNL_CB_ERROR;
    }

    ph = mnl_attr_get_payload(attr[NFQA_PACKET_HDR]);

    plen = mnl_attr_get_payload_len(attr[NFQA_PAYLOAD]);
    void *payload = mnl_attr_get_payload(attr[NFQA_PAYLOAD]);

    int result = modbus_filter(payload);
    mysql_result.result = (result==NF_DROP?0:1);
    if (bypass_log != 1 && save_log != 0)
    {
        if (result == NF_DROP || testmode == 1)
        {
            write_result(&mysql_result);
        }
    }

    skbinfo = attr[NFQA_SKB_INFO] ? ntohl(mnl_attr_get_u32(attr[NFQA_SKB_INFO])) : 0;

    if (attr[NFQA_CAP_LEN])
    {
        uint32_t orig_len = ntohl(mnl_attr_get_u32(attr[NFQA_CAP_LEN]));
        if (orig_len != plen)
            printf("truncated ");
    }

    if (skbinfo & NFQA_SKB_GSO)
        printf("GSO ");

    id = ntohl(ph->packet_id);
    //printf("packet received (id=%u hw=0x%04x hook=%u, payload len %u",
    //id, ntohs(ph->hw_protocol), ph->hook, plen);

    /*
     * ip/tcp checksums are not yet valid, e.g. due to GRO/GSO.
     * The application should behave as if the checksums are correct.
     *
     * If these packets are later forwarded/sent out, the checksums will
     * be corrected by kernel/hardware.
     */
    if (skbinfo & NFQA_SKB_CSUMNOTREADY)
        printf(", checksum not ready");
    //puts(")");

    nfq_send_verdict(ntohs(nfg->res_id), id, result);

    return MNL_CB_OK;
}

int main(int argc, char *argv[])
{
    char *buf;
    /* largest possible packet payload, plus netlink data overhead: */
    size_t sizeof_buf = 0xffff + (MNL_SOCKET_BUFFER_SIZE/2);
    struct nlmsghdr *nlh;
    int ret;
    unsigned int portid, queue_num;

    if (argc != 2)
    {
        printf("Usage: %s [queue_num]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    queue_num = atoi(argv[1]);

    nl = mnl_socket_open(NETLINK_NETFILTER);
    if (nl == NULL)
    {
        perror("mnl_socket_open");
        exit(EXIT_FAILURE);
    }

    if (mnl_socket_bind(nl, 0, MNL_SOCKET_AUTOPID) < 0)
    {
        perror("mnl_socket_bind");
        exit(EXIT_FAILURE);
    }
    portid = mnl_socket_get_portid(nl);

    buf = malloc(sizeof_buf);
    if (!buf)
    {
        perror("allocate receive buffer");
        exit(EXIT_FAILURE);
    }

    /* PF_(UN)BIND is not needed with kernels 3.8 and later */
    nlh = nfq_hdr_put(buf, NFQNL_MSG_CONFIG, 0);
    nfq_nlmsg_cfg_put_cmd(nlh, AF_INET, NFQNL_CFG_CMD_PF_UNBIND);

    if (mnl_socket_sendto(nl, nlh, nlh->nlmsg_len) < 0)
    {
        perror("mnl_socket_send");
        exit(EXIT_FAILURE);
    }

    nlh = nfq_hdr_put(buf, NFQNL_MSG_CONFIG, 0);
    nfq_nlmsg_cfg_put_cmd(nlh, AF_INET, NFQNL_CFG_CMD_PF_BIND);

    if (mnl_socket_sendto(nl, nlh, nlh->nlmsg_len) < 0)
    {
        perror("mnl_socket_send");
        exit(EXIT_FAILURE);
    }

    nlh = nfq_hdr_put(buf, NFQNL_MSG_CONFIG, queue_num);
    nfq_nlmsg_cfg_put_cmd(nlh, AF_INET, NFQNL_CFG_CMD_BIND);

    if (mnl_socket_sendto(nl, nlh, nlh->nlmsg_len) < 0)
    {
        perror("mnl_socket_send");
        exit(EXIT_FAILURE);
    }

    nlh = nfq_hdr_put(buf, NFQNL_MSG_CONFIG, queue_num);
    nfq_nlmsg_cfg_put_params(nlh, NFQNL_COPY_PACKET, 0xffff);

    mnl_attr_put_u32(nlh, NFQA_CFG_FLAGS, htonl(NFQA_CFG_F_GSO));
    mnl_attr_put_u32(nlh, NFQA_CFG_MASK, htonl(NFQA_CFG_F_GSO));

    if (mnl_socket_sendto(nl, nlh, nlh->nlmsg_len) < 0)
    {
        perror("mnl_socket_send");
        exit(EXIT_FAILURE);
    }

    /* ENOBUFS is signalled to userspace when packets were lost
     * on kernel side.  In most cases, userspace isn't interested
     * in this information, so turn it off.
     */
    ret = 1;
    mnl_socket_setsockopt(nl, NETLINK_NO_ENOBUFS, &ret, sizeof(int));

    // read config
    //init_mysql();
    dev_id = queue_num - 5000;
    read_config(&testmode, &save_log, dev_id, &op_cnt, &addr_cnt, op_white_list, &(addr_control_list[0]));
    mysql_result.protocol = PROTOCOL_MODBUS;
    if (dev_id < 100)
    {
        strcpy(mysql_result.dest, "/dev/ttyM ");
        mysql_result.dest[strlen(mysql_result.dest)-1] = dev_id + '0';
    }
    for (;;)
    {
        ret = mnl_socket_recvfrom(nl, buf, sizeof_buf);
        if (ret == -1)
        {
            perror("mnl_socket_recvfrom");
            exit(EXIT_FAILURE);
        }

        ret = mnl_cb_run(buf, ret, 0, portid, queue_cb, NULL);
        if (ret < 0)
        {
            perror("mnl_cb_run");
            exit(EXIT_FAILURE);
        }
    }

    mnl_socket_close(nl);

    return 0;
}

int modbus_filter(void* payload)
{
    bypass_log = 1;
    struct iphdr *iphdr = (struct iphdr *)payload;
    struct tcphdr *tcphdr = NULL;
    void *tail = NULL;
    struct in_addr ia;
    if (iphdr->version == 4)
    {
        ia.s_addr = iphdr->saddr;
        strcpy(mysql_result.source, inet_ntoa(ia));

        if (dev_id > 99)
        {
            ia.s_addr = iphdr->daddr;
            strcpy(mysql_result.dest, inet_ntoa(ia));
        }
        tcphdr = payload + iphdr->ihl*4;
        tail = payload + ntohs(iphdr->tot_len);
    }
    else
    {
        inet_ntop(AF_INET6, &((struct ipv6hdr*)payload)->saddr, mysql_result.source, 254);

        if (dev_id > 99)
        {
            inet_ntop(AF_INET6, &((struct ipv6hdr*)payload)->daddr, mysql_result.dest, 254);
        }
        tcphdr = payload + sizeof(struct ipv6hdr);
        tail = (unsigned char*)tcphdr + ntohs(((struct ipv6hdr*)payload)->payload_len);
    }

    struct modbus_adu *modbus_adu = (struct modbus_adu *)((void*)tcphdr + tcphdr->doff*4);
    mysql_result.data = &(modbus_adu->uid);
    mysql_result.data_len = ntohs((modbus_adu->len))>60?60:ntohs((modbus_adu->len));

    //packet check
    if (modbus_adu->pid != 0)
    {
        return NF_ACCEPT;
    }
    if ((void*)&(modbus_adu->pdu.op_code) > tail)
    {
        return NF_ACCEPT;
    }

    bypass_log = 0;
    //op code filter
    int pass = 0;
    int op_i = 0;
    for (op_i = 0; op_i < op_cnt; op_i++)
    {
        if (modbus_adu->pdu.op_code == op_white_list[op_i])
        {
            pass = 1;
            break;
        }
    }

    if (pass == 0)
    {
        return NF_DROP;
    }

    unsigned short read_addr_start = 0;
    unsigned char read_count = 0;
    unsigned short write_addr_start = 0;
    unsigned char write_count = 0;
    unsigned short *write_data_p = NULL;
    unsigned int mode = 0;
    switch (modbus_adu->pdu.op_code)
    {
    case 0x03:
    case 0x04:
        mode = MODBUS_READ;
        read_addr_start = ntohs(((struct op_03_04*)(modbus_adu->pdu.data))->start_addr);
        read_count = ntohs(((struct op_03_04*)(modbus_adu->pdu.data))->reg_count);
        break;
    case 0x06:
        mode = MODBUS_WRITE;
        write_addr_start = ntohs(((struct op_06*)(modbus_adu->pdu.data))->addr);
        write_count = 1;
        write_data_p = &(((struct op_06*)(modbus_adu->pdu.data))->val);
        break;
    case 0x10:
        mode = MODBUS_WRITE;
        write_addr_start = ntohs(((struct op_10*)(modbus_adu->pdu.data))->start_addr);
        write_count = ntohs(((struct op_10*)(modbus_adu->pdu.data))->reg_count);
        write_data_p = ((struct op_10*)(modbus_adu->pdu.data))->data;
        break;
    case 0x17:
        mode = MODBUS_READ | MODBUS_WRITE;
        read_addr_start = ntohs(((struct op_17*)(modbus_adu->pdu.data))->read_start_addr);
        read_count = ntohs(((struct op_17*)(modbus_adu->pdu.data))->read_reg_count);
        write_addr_start = ntohs(((struct op_17*)(modbus_adu->pdu.data))->write_start_addr);
        write_count = ntohs(((struct op_17*)(modbus_adu->pdu.data))->write_reg_count);
        write_data_p = ((struct op_17*)(modbus_adu->pdu.data))->write_data;
        break;
    default:
        break;
    }

    // read register address filter
    int i,j;
    unsigned short addr;
    if (mode & MODBUS_READ)
    {
        for (i = 0; i < read_count; i++)
        {
            addr = read_addr_start + i;

            pass = 0;
            for (j = 0; j < addr_cnt && pass == 0; j++)
            {
                struct addr_control *addr_filter = &addr_control_list[j];

                if (addr_filter->addr == addr)
                {
                    if (addr_filter->control & MODBUS_READ)
                    {
                        pass = 1;
                        break;
                    }
                    else
                    {
                        return NF_DROP;
                    }
                }
            }
            if (pass == 0)
            {
                return NF_DROP;
            }
        }
    }

    // write register address filter
    if (mode & MODBUS_WRITE)
    {
        for (i = 0; i < write_count; i++)
        {
            addr = write_addr_start + i;

            pass = 0;
            for (j = 0; j < addr_cnt && pass == 0; j++)
            {
                struct addr_control *addr_filter = &addr_control_list[j];
                if (addr_filter->addr == addr)
                {
                    if (addr_filter->control & MODBUS_WRITE == 0)
                    {
                        return NF_DROP;
                    }
                    if (addr_filter->control & MODBUS_WRITE_LOW)
                    {
                        unsigned short data = ntohs(write_data_p[i]);
                        if (data < addr_filter->low)
                        {
                            return NF_DROP;
                        }
                    }
                    if (addr_filter->control & MODBUS_WRITE_HIGH)
                    {
                        unsigned short data = ntohs(write_data_p[i]);
                        if (data > addr_filter->high)
                        {
                            return NF_DROP;
                        }
                    }

                    pass = 1;
                }
            }
            if (pass == 0)
            {
                return NF_DROP;
            }
        }
    }

    return NF_ACCEPT;
}
