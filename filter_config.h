#ifndef FILTER_CONFIG_H_INCLUDED
#define FILTER_CONFIG_H_INCLUDED

#include "gdw3761_filter.h"
#include "dnp_3_0_filter.h"
#include "bacnet_ip_filter.h"
#include "ethernet_ip_filter.h"

#define OP_WHITE_LIST_MAX 255
#define ADDR_CONTROL_LIST_MAX 511

#define MODBUS_READ  (1 << 1 )
#define MODBUS_WRITE (1 << 2 )
#define MODBUS_WRITE_LOW  (1 << 3 )
#define MODBUS_WRITE_HIGH (1 << 4 )

#define PROTOCOL_MODBUS 1
#define PROTOCOL_CJT_188 2
#define PROTOCOL_DLT_645 3
#define PROTOCOL_GDW_3761 4
#define PROTOCOL_DNP_3 5
#define PROTOCOL_BACNET 6
#define PROTOCOL_ETHERNET 7

struct addr_control
{
    unsigned short addr;
    unsigned char control;
    unsigned short low;
    unsigned short high;
};

struct audit_result
{
    char source[255];
    char dest[255];
    int data_len;
    int protocol;
    unsigned char *data;
    int result;
};

extern void init_mysql();
extern int read_config(int *testmode, int* save_log, int dev_id, int *op_cnt, int *addr_cnt, unsigned char *op_list, struct addr_control *addr_control_list);
extern int write_result(struct audit_result *result);
extern int read_gdw_config(int *testmode, int* save_log, int dev_id, int *op_cnt, GDWFilterType* p_op_list);
extern int read_dnp_config(int *testmode, int* save_log, int dev_id, int *op_cnt, DNP3FilterType* p_op_list);
extern int read_bacnet_config(int *testmode, int* save_log, int dev_id, int *op_cnt, BACNetIPFilterType* p_op_list);
extern int read_ethernet_config(int *testmode, int* save_log, int dev_id, int *op_cnt, EthernetIPFilterType* p_op_list);
extern int write_dos_alarm(const char *source_ip_str);

#endif // FILTER_CONFIG_H_INCLUDED
