#include "bacnet_ip_filter.h"
#include <stdlib.h>
#include <string.h>

typedef unsigned int                u32;    /*!<u32*/
typedef unsigned short              u16;    /*!<u16*/
typedef short                       s16;    /*!<u16*/
typedef unsigned char               u8;     /*!<u8*/
typedef enum
{
    FALSE = 0,      /*!<FALSE，假*/
    TRUE = !FALSE   /*!<TRUE，真*/
}bool;
 
#pragma pack(1)
typedef struct BACNetIPType_st
{
    struct 
    {
        u8  Type;       /*类型*/
        u8  Func;       /*功能*/
        u16 Len;        /*长度*/
    }Bpdu;
    struct
    {
        u8 Version;     /*版本*/
        u8 Control;     /*控制*/
        u8 DNET[2];     /*最终目标网络号码*/
        u8 DLEN;        /*最终目标MAC层地址长度*/
        u8 DADR[6];     /*最终目标MAC层地址*/
        u8 SNET[2];     /*源网络号码*/
        u8 SLEN;        /*源MAC层地址长度*/
        u8 SADR[6];     /*源MAC层地址*/
        u8 HopCount;    /*转发计数*/
        u8 MessageType; /*报文类型*/
        u16 VendorId;   /*生厂商标示符*/
    }Npdu;
    u8 PduType;
}BACNetIPType;
#pragma pack()

static BACNetIPFilterType* BacNetFilterList = NULL;
static int BacNetFilterNumber = 0;

#define MyMallloc   malloc
#define MyFree      free
#define MyMemCpy    memcpy

#ifndef WIN32
// #define MySemStruct             struct semaphore
// #define MySemInit(sem_ptr)      init_MUTEX_LOCKED(sem_ptr)
// #define MySemWait(sem_ptr)      down(sem_ptr)
// #define MySemPost(sem_ptr)      up(sem_ptr)

#include<semaphore.h>
#define MySemStruct             sem_t
#define MySemInit(sem_ptr)      sem_init(sem_ptr, 0, 1)
#define MySemWait(sem_ptr)      sem_wait(sem_ptr)
#define MySemPost(sem_ptr)      sem_post(sem_ptr)
#else

#define MySemStruct         int
#define MySemInit(sem_ptr)
#define MySemWait(sem_ptr)
#define MySemPost(sem_ptr)

#endif // WIN32


static MySemStruct FilterListSem;

//BVLL Function definitions
#define Write_Broadcast_Distribution_Table     0x01
#define Read_Broadcast_Distribution_Table      0x02
#define Read_Broadcast_Distribution_Table_ACK  0x03
#define Forwarded_NPDU                         0x04
#define Register_Foreign_Device                0x05
#define Read_Foreign_Device_Table              0x06
#define Read_Foreign_Device_Table_ACK          0x07
#define Delete_Foreign_Device_Table_Entry      0x08
#define Distribute_Broadcast_To_Network        0x09
#define Original_Unicast_NPDU                  0x0A
#define Original_Broadcast_NPDU                0x0B

// note: these are not the real values,
// but are shifted left cause I am lazy
typedef enum {
    PDU_TYPE_CONFIRMED_SERVICE_REQUEST = 0,
    PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST = 0x01,
    PDU_TYPE_SIMPLE_ACK = 0x02,
    PDU_TYPE_COMPLEX_ACK = 0x03,
    PDU_TYPE_SEGMENT_ACK = 0x04,
    PDU_TYPE_ERROR = 0x05,
    PDU_TYPE_REJECT = 0x06,
    PDU_TYPE_ABORT = 0x07
} BACNET_PDU_TYPE;

typedef enum {
    // Alarm and Event Services
    SERVICE_CONFIRMED_ACKNOWLEDGE_ALARM = 0,
    SERVICE_CONFIRMED_COV_NOTIFICATION = 1,
    SERVICE_CONFIRMED_EVENT_NOTIFICATION = 2,
    SERVICE_CONFIRMED_GET_ALARM_SUMMARY = 3,
    SERVICE_CONFIRMED_GET_ENROLLMENT_SUMMARY = 4,
    SERVICE_CONFIRMED_GET_EVENT_INFORMATION = 29,
    SERVICE_CONFIRMED_SUBSCRIBE_COV = 5,
    SERVICE_CONFIRMED_SUBSCRIBE_COV_PROPERTY = 28,
    SERVICE_CONFIRMED_LIFE_SAFETY_OPERATION = 27,
    // File Access Services
    SERVICE_CONFIRMED_ATOMIC_READ_FILE = 6,
    SERVICE_CONFIRMED_ATOMIC_WRITE_FILE = 7,
    // Object Access Services
    SERVICE_CONFIRMED_ADD_LIST_ELEMENT = 8,
    SERVICE_CONFIRMED_REMOVE_LIST_ELEMENT = 9,
    SERVICE_CONFIRMED_CREATE_OBJECT = 10,
    SERVICE_CONFIRMED_DELETE_OBJECT = 11,
    SERVICE_CONFIRMED_READ_PROPERTY = 12,
    SERVICE_CONFIRMED_READ_PROPERTY_CONDITIONAL = 13,
    SERVICE_CONFIRMED_READ_PROPERTY_MULTIPLE = 14,
    SERVICE_CONFIRMED_READ_RANGE = 26,
    SERVICE_CONFIRMED_WRITE_PROPERTY = 15,
    SERVICE_CONFIRMED_WRITE_PROPERTY_MULTIPLE = 16,
    // Remote Device Management Services
    SERVICE_CONFIRMED_DEVICE_COMMUNICATION_CONTROL = 17,
    SERVICE_CONFIRMED_PRIVATE_TRANSFER = 18,
    SERVICE_CONFIRMED_TEXT_MESSAGE = 19,
    SERVICE_CONFIRMED_REINITIALIZE_DEVICE = 20,
    // Virtual Terminal Services
    SERVICE_CONFIRMED_VT_OPEN = 21,
    SERVICE_CONFIRMED_VT_CLOSE = 22,
    SERVICE_CONFIRMED_VT_DATA = 23,
    // Security Services
    SERVICE_CONFIRMED_AUTHENTICATE = 24,
    SERVICE_CONFIRMED_REQUEST_KEY = 25
    // Services added after 1995
    // readRange (26) see Object Access Services
    // lifeSafetyOperation (27) see Alarm and Event Services
    // subscribeCOVProperty (28) see Alarm and Event Services
    // getEventInformation (29) see Alarm and Event Services
} BACNET_CONFIRMED_SERVICE;

typedef enum {
    SERVICE_UNCONFIRMED_I_AM = 0,
    SERVICE_UNCONFIRMED_I_HAVE = 1,
    SERVICE_UNCONFIRMED_COV_NOTIFICATION = 2,
    SERVICE_UNCONFIRMED_EVENT_NOTIFICATION = 3,
    SERVICE_UNCONFIRMED_PRIVATE_TRANSFER = 4,
    SERVICE_UNCONFIRMED_TEXT_MESSAGE = 5,
    SERVICE_UNCONFIRMED_TIME_SYNCHRONIZATION = 6,
    SERVICE_UNCONFIRMED_WHO_HAS = 7,
    SERVICE_UNCONFIRMED_WHO_IS = 8,
    SERVICE_UNCONFIRMED_UTC_TIME_SYNCHRONIZATION = 9
    // Other services to be added as they are defined.
    // All choice values in this production are reserved
    // for definition by ASHRAE.
    // Proprietary extensions are made by using the
    // UnconfirmedPrivateTransfer service. See Clause 23.
} BACNET_UNCONFIRMED_SERVICE;

/***************************************************************************************************
*\Function      GetBACNetProtocolContent
*\Description   从IP包中获取BACNet协议内容
*\Parameter     ip      IP包ip
*\Parameter     ip_len  IP包长度
*\Parameter     bac_net 获取后的bac_net包
*\Parameter     length  bac_net包长度
*\Return        bool
*\Note          
***************************************************************************************************/
static bool GetBACNetProtocolContent(u8* ip, u32 ip_len, u8** bac_net, u32* length)
{
    /*先去掉IP头*/
    u32 len = (ip[0] & 0x0f) * 4;   /*计算首部长度*/
    u8* tcpudp = (u8*)(ip + len);
    u8 protocol = ip[9];
    len = (ip[2])*256 + ip[3] - len;

    /*再去掉TCP头或UDP头*/
    if (protocol == 6)
    {
        *bac_net = tcpudp + (tcpudp[12]>>4) * 4;
        *length = len - (tcpudp[12]>>4) * 4;
    }
    else if (protocol == 17)
    {
        *bac_net = tcpudp + 8;
        *length = (tcpudp[4]*256+tcpudp[5] - 8);
    }
    else
    {
        return FALSE;
    }

    return TRUE;
}

/***************************************************************************************************
*\Function      FilterMatch
*\Description   遍历过滤规则,判断合法性
*\Parameter     type            有证实/无证实服务
*\Parameter     service_choice  服务号
*\Return        bool            是否合法
*\Note          
***************************************************************************************************/
static bool FilterMatch(BACNET_PDU_TYPE type, u8 service_choice, u8 tag, u16 object, u32 instance)
{
    int i = 0;
    bool re = FALSE;

    if (BacNetFilterNumber == 0 || BacNetFilterList == NULL)
    {
        return FALSE;
    }
    /*其他类型数据帧直接放行*/
    if((type != PDU_TYPE_CONFIRMED_SERVICE_REQUEST) && (type != PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST))
    {
        return TRUE;
    }

    /***************************************************************************************************
    *互斥
    ***************************************************************************************************/
    MySemWait(&FilterListSem);

    for (i = 0; i < BacNetFilterNumber; i++)
    {
        /*判断Sercive类型和ServiceNumber是否符合规则*/
        if((BacNetFilterList[i].ServiceType == type) && \
           (service_choice >= BacNetFilterList[i].ServiceNum_Start) && \
           (service_choice <= BacNetFilterList[i].ServiceNum_End))
        {
            if((service_choice < 6) || (service_choice > 9 && service_choice < 12) || (service_choice == 13) || (service_choice > 16))
            {
                /*有证实的服务过滤且服务号属于：06、07、08、09、12、14、15、16的前提下才执行之后的过滤*/
                MySemPost(&FilterListSem);
                return TRUE;
            }
            if(tag & 0xF0 != 0xC0)
            {
                /*若TAG高4位不是0xC0则直接放行*/
                MySemPost(&FilterListSem);
                return TRUE;
            }
            /*Object过滤是否使能*/
            if(BacNetFilterList[i].ObjectEnable == 1)
            {
                /*object是否在规则区域内*/
                if((object >= BacNetFilterList[i].ObjectNum_Start) && (object <= BacNetFilterList[i].ObjectNum_End))
                {
                    /*Instance过滤是否使能*/
                    if(BacNetFilterList[i].InstanceEnable == 1)
                    {
                        /*Instance是否在规则区域内*/
                        if(!((instance >= BacNetFilterList[i].InstanceNum_Start) && (instance <= BacNetFilterList[i].InstanceNum_End)))
                        {
                            /*不符合此规则要求，进行下一条验证规则*/
                            continue;
                        }
                    }
                }
                else
                {
                    /*不符合此规则要求，进行下一条验证规则*/
                    continue;
                }
            }
            
            MySemPost(&FilterListSem);
            return TRUE;
        }
    }

    /***************************************************************************************************
    *互斥结束
    ***************************************************************************************************/
    MySemPost(&FilterListSem);

    return FALSE;
}

int decode_unsigned32(
    u8* apdu,
    u32* value)
{
    if (value) {
        *value = ((u32) ((((u32) apdu[0]) << 24) & 0xff000000));
        *value |= ((u32) ((((u32) apdu[1]) << 16) & 0x00ff0000));
        *value |= ((u32) ((((u32) apdu[2]) << 8) & 0x0000ff00));
        *value |= ((u32) (((u32) apdu[3]) & 0x000000ff));
    }

    return 4;
}

/***************************************************************************************************
*\Function      BacnetIpAnalysis
*\Description   BACnet/IP协议解析
*\Parameter     buf     待解析数据
*\Parameter     len     待解析数据长度
*\Return        bool    解析结果
*\Note          
***************************************************************************************************/
bool BacnetIpAnalysis(u8* buf, u32 len)
{
    BACNetIPType* adu = (BACNetIPType*)buf;
    u8 service_type = 0;
    u8 service_choice = 0;
    u8 segmented_message = 0;
    u8* apdu = NULL;
    u16 tmp_h = 0;
    u16 tmp_l = 0;
    u8  tag = 0;
    u16 object = 0;
    u32 instance = 0;
    if(len < sizeof(BACNetIPType))  /*报文长度太短*/
    {
        return FALSE;
    }
    tmp_h = adu->Bpdu.Len<<8;
    tmp_l = adu->Bpdu.Len>>8;
    adu->Bpdu.Len =  tmp_h + tmp_l;
    /*报文正确性验证*/
    if(adu->Bpdu.Len > len - sizeof(adu->Bpdu))
    {
        /*数据帧不完整或解析错误*/
        return FALSE;
    }

    if(adu->Bpdu.Type != 0x81)
    {
        /*类型不符合*/
        return FALSE;
    }

    if ((adu->Bpdu.Func != Original_Unicast_NPDU)
        && (adu->Bpdu.Func != Original_Broadcast_NPDU))
    {
        /*类型不符合*/
        return FALSE;
    }


    service_type = adu->PduType & 0xF0;
    segmented_message = (adu->PduType & 0x08) ? 1 : 0;
    apdu = &adu->PduType;

    switch(service_type>>4)
    {
        case PDU_TYPE_CONFIRMED_SERVICE_REQUEST:    /*有证实的服务*/
            if (segmented_message) {
                service_choice = apdu[5];
                tag = apdu[6];
                decode_unsigned32(&apdu[7], &instance);
                object = instance>>22;
                instance = instance & 0x3FFFFF;
            } else {
                service_choice = apdu[3];
                tag = apdu[4];
                decode_unsigned32(&apdu[5], &instance);
                object = instance>>22;
                instance = instance & 0x3FFFFF;
            }
            break;
        case PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST:  /*无证实的服务*/
            service_choice = apdu[1];
            break;
        default:
            break;
    }
    
    /*报文合法性验证*/
    if (FilterMatch((BACNET_PDU_TYPE)(service_type>>4), service_choice, tag, object, instance) == FALSE)
    {
        /*返回验证结果*/
        return FALSE;
    }
    /*返回验证结果*/
    return TRUE;
}

/***************************************************************************************************
*\Function      BACnetIPFilterInit
*\Description   BACNet/IP协议过滤初始化
*\Parameter     filter_list 协议过滤规则列表，类型为BACNetIPFilterType类型的数组
*\Parameter     num         协议过滤规则数目
*\Return        int         是否初始化成功，0：成功，1：失败
*\Note          
***************************************************************************************************/
int BACnetIPFilterInit(BACNetIPFilterType * filter_list, int num)
{
    static bool once_run = TRUE;

    if (once_run == TRUE)
    {
        once_run = FALSE;
        MySemInit(&FilterListSem);
    }

    /*安检*/
    if (filter_list == NULL || num == 0)
    {
        return 1;
    }

    /***************************************************************************************************
    *互斥
    ***************************************************************************************************/
    MySemWait(&FilterListSem);

    /*释放旧的规则*/
    if ((BacNetFilterList != NULL) && (BacNetFilterNumber != 0))
    {
        MyFree(BacNetFilterList);
    }

    BacNetFilterList = (BACNetIPFilterType*)MyMallloc(sizeof(BACNetIPFilterType) * num);
    BacNetFilterNumber = num;

    MyMemCpy(BacNetFilterList, filter_list, sizeof(BACNetIPFilterType) * num);
    /***************************************************************************************************
    *互斥结束
    ***************************************************************************************************/
    MySemPost(&FilterListSem);

    return 0;
}

/***************************************************************************************************
*\Function      BACnetIPFilterCheck
*\Description   BACNet/IP协议数据包过滤检查
*\Parameter     ip_packet_buf           IP层数据包
*\Parameter     ip_packet_buf_length    IP数据包长度
*\Return        int                     该数据包是否可通过,0:通过，1：不能通过
*\Note          
***************************************************************************************************/
int BACnetIPFilterCheck(char* ip_packet_buf, int ip_packet_buf_length)
{
    u8* bac_net = NULL;
    u32 bac_net_len = 0;

    if (ip_packet_buf == NULL || ip_packet_buf_length == 0)
    {
        return 1;
    }

    if (GetBACNetProtocolContent((u8*)ip_packet_buf, ip_packet_buf_length, &bac_net, &bac_net_len) == FALSE)
    {
        return 1;
    }

    if (BacnetIpAnalysis(bac_net, bac_net_len) == FALSE)
    {
        return 1;
    }

    return 0;
}
