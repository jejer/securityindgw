#include "gdw3761_filter.h"


typedef unsigned int                u32;    /*!<u32*/
typedef unsigned short              u16;    /*!<u16*/
typedef short                       s16;    /*!<u16*/
typedef unsigned char               u8;     /*!<u8*/
typedef enum
{
    FALSE = 0,      /*!<FALSE，假*/
    TRUE = !FALSE   /*!<TRUE，真*/
}bool;

#include <stdlib.h>
#include <string.h>

#define MyMallloc   malloc
#define MyFree      free
#define MyMemCpy    memcpy

#ifndef WIN32
// #define MySemStruct             struct semaphore
// #define MySemInit(sem_ptr)      init_MUTEX_LOCKED(sem_ptr)
// #define MySemWait(sem_ptr)      down(sem_ptr)
// #define MySemPost(sem_ptr)      up(sem_ptr)

#include<semaphore.h>
#define MySemStruct             sem_t
#define MySemInit(sem_ptr)      sem_init(sem_ptr, 0, 1)
#define MySemWait(sem_ptr)      sem_wait(sem_ptr)
#define MySemPost(sem_ptr)      sem_post(sem_ptr)
#else

#define MySemStruct         int
#define MySemInit(sem_ptr)
#define MySemWait(sem_ptr)
#define MySemPost(sem_ptr)

#endif // WIN32





static GDWFilterType* GDWFilterList = NULL;
static int GDWFilterNumber = 0;

static MySemStruct FilterListSem;

static bool GDWAnalysis(u8* buf, u32 len);
static bool GetGDWProtocolContent(u8* ip, u32 ip_len, u8** gdw, u32* length);
static bool FilterMatch(u16 value, u16 pn, u16 fn);
static u8 MemSum(u8* ptr, u32 length);

/***************************************************************************************************
*\Function      GDWFilterInit
*\Description   国网规约协议过滤初始化
*\Parameter     filter_list 协议过滤规则列表，类型为GDWFilterAFNType类型的数组
*\Parameter     num         协议过滤规则数目
*\Return        int         是否初始化成功，0：成功，1：失败
*\Note
*               创建函数。
***************************************************************************************************/
int GDWFilterInit(GDWFilterType* filter_list, int num)
{
    static bool once_run = TRUE;

    if (once_run == TRUE)
    {
        once_run = FALSE;
        MySemInit(&FilterListSem);
    }

    /*安检*/
    if (filter_list == NULL || num == 0)
    {
        return 1;
    }

    /***************************************************************************************************
    *互斥
    ***************************************************************************************************/
    MySemWait(&FilterListSem);
     
    /*释放旧的规则*/
    if ((GDWFilterList != NULL) && (GDWFilterNumber != 0))
    {
        MyFree(GDWFilterList);
    }

    GDWFilterList = (GDWFilterType*)MyMallloc(sizeof(GDWFilterType) * num);
    GDWFilterNumber = num;

    MyMemCpy(GDWFilterList, filter_list, sizeof(GDWFilterType) * num);
    /***************************************************************************************************
    *互斥结束
    ***************************************************************************************************/
    MySemPost(&FilterListSem);

    return 0;
}

/***************************************************************************************************
*\Function      GDWFilterCheck
*\Description   GDW协议数据包过滤检查
*\Parameter     ip_packet_buf           IP层数据包
*\Parameter     ip_packet_buf_length    IP数据包长度
*\Return        int                     该数据包是否可通过,0:通过，1：不能通过
*\Note
***************************************************************************************************/
int GDWFilterCheck(char* ip_packet_buf, int ip_packet_buf_length)
{
    u8* gdw = NULL;
    u32 gdw_len = 0;

    if (ip_packet_buf == NULL || ip_packet_buf_length == 0)
    {
        return 1;
    }

    if (GetGDWProtocolContent((u8*)ip_packet_buf, ip_packet_buf_length, &gdw, &gdw_len) == FALSE)
    {
        return 1;
    }

    if (GDWAnalysis(gdw, gdw_len) == FALSE)
    {
        return 1;
    }

    return 0;
}

/***************************************************************************************************
*\Function      GDWAnalysis
*\Description   GDW解析
*\Parameter     buf     待解析数据
*\Parameter     len     待解析数据长度
*\Return        bool    数据分析结果
*\Note
***************************************************************************************************/
static bool GDWAnalysis(u8* buf, u32 len)
{
    u32 data_len = 0;
    u8 cs = 0;
    u8 afn = 0;
    u16 pn = 0;
    u16 fn = 0;

    if (len < 1)
    {
        return FALSE;
    }

    /*协议正确性检查*/
    if (buf[0] != 0x68 || buf[5] != 0x68)
    {
        return FALSE;
    }

    /*长度检查*/
    if (buf[1] != buf[3] || buf[2] != buf[4])
    {
        return FALSE;
    }
    data_len = *(u16*)(buf+1);
    data_len >>= 2;
	

    /*CS校验，及结尾标识验证*/
    if ((data_len + 8) <= len)
    {
        /*取出校验和*/
        cs = buf[data_len+6];

        if (buf[data_len+7] != 0x16)
        {
            /*结尾不合法*/
            return FALSE;
        }

        if (cs != MemSum(buf+6, data_len))
        {
            /*校验不通过*/
            return FALSE;
        }
    }

    /*过滤*/
    if (FilterMatch(buf[12], *(u16*)&buf[14], *(u16*)&buf[16]) == FALSE)
    {
        return FALSE;
    }

    return TRUE;
}

/***************************************************************************************************
*\Function      MemSum
*\Description   校验和计算函数
*\Parameter     ptr     数据缓冲区
*\Parameter     length  数据缓冲区大小
*\Return        u8      校验和
*\Note
***************************************************************************************************/
static u8 MemSum(u8* ptr, u32 length)
{
    u32 i = 0;
    u8 checksum  = 0;
    for (i = 0; i < length; i++)
    {
        checksum += ptr[i];
    }

    return checksum;
}

/***************************************************************************************************
*\Function      GetGDWProtocolContent
*\Description   从IP包中获取国网规约协议内容
*\Parameter     ip
*\Parameter     ip_len
*\Parameter     gdw
*\Parameter     length
*\Return        bool
*\Note
***************************************************************************************************/
static bool GetGDWProtocolContent(u8* ip, u32 ip_len, u8** gdw, u32* length)
{
    /*先去掉IP头*/
    u32 len = (ip[0] & 0x0f) * 4;   /*计算首部长度*/
    u8* tcpudp = (u8*)(ip + len);
    u8 protocol = ip[9];
    len = (ip[2])*256 + ip[3] - len;

    /*再去掉TCP头或UDP头*/
    if (protocol == 6)
    {
        *gdw = tcpudp + (tcpudp[12]>>4) * 4;
        *length = len - (tcpudp[12]>>4) * 4;
    }
    else if (protocol == 17)
    {
        *gdw = tcpudp + 8;
        *length = (tcpudp[4]*256+tcpudp[5] - 8);
    }
    else
    {
        return FALSE;
    }

    return TRUE;
}

/***************************************************************************************************
*\Function      FilterMatch
*\Description   遍历过滤规则，看是否可以通过
*\Parameter     value   要过滤的值
*\Return        bool    是否通过
*\Note
***************************************************************************************************/
static bool FilterMatch(u16 afn, u16 pn, u16 fn)
{
    int i = 0;

    if ((GDWFilterNumber == 0) || (GDWFilterList == NULL))
    {
        return FALSE;
    }

    /***************************************************************************************************
    *互斥
    ***************************************************************************************************/
    MySemWait(&FilterListSem);

    /*因为冲突时以最后设置的规则为准，因此从后往前遍历规则*/
    for (i = GDWFilterNumber - 1; i >= 0; i--)
    {
        /*判断afn是否合法*/
        if ((afn >= GDWFilterList[i].AFN_Start) && (afn <= GDWFilterList[i].AFN_End))
        {            
            /*判断是否启用pn过滤*/
            if (GDWFilterList[i].PnEnable == 1)
            {
                /*判断pn是否合法*/
                if ((pn >= GDWFilterList[i].Pn_Start) && (pn <= GDWFilterList[i].Pn_End))
                {
                    /*判断是否启用fn过滤*/
                    if (GDWFilterList[i].FnEnable == 1)
                    {
                        /*判断fn是否合法*/
                        if (!((fn >= GDWFilterList[i].Fn_Start) && (fn <= GDWFilterList[i].Fn_End)))
                        {
                            /*fn不符合过滤规则*/
                            continue;
                        }
                    }
                }
                else
                {
                    /*pn不符合过滤规则*/
                    continue;
                }
            }

            MySemPost(&FilterListSem);
            return TRUE;
        }
    }

    /***************************************************************************************************
    *互斥结束
    ***************************************************************************************************/
    MySemPost(&FilterListSem);

    return FALSE;
}
