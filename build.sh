#!/bin/sh
set -x
gcc -c rw_mysql_config.c -o rw_mysql_config.o `mysql_config --cflags --libs`
gcc modbus_filter.c rw_mysql_config.o -o modbus_filter -DHHMYSQL -lnetfilter_queue -lmnl `mysql_config --cflags --libs`
gcc cjt_gw.c rw_mysql_config.o -o cjt_gw -DHHMYSQL `mysql_config --cflags --libs`
gcc dlt_gw.c rw_mysql_config.o -o dlt_gw -DHHMYSQL `mysql_config --cflags --libs`
gcc dos_detector.c rw_mysql_config.o -o dos_detector -DHHMYSQL -lnetfilter_queue -lmnl `mysql_config --cflags --libs`
gcc -c gdw3761_filter.c -o gdw3761_filter.o -g
gcc -c dnp_3_0_filter.c -o dnp_3_0_filter.o -g
gcc -c bacnet_ip_filter.c -o bacnet_ip_filter.o -g
gcc -c ethernet_ip_filter.c -o ethernet_ip_filter.o -g
gcc -c rw_mysql_config.c -o rw_mysql_config.o `mysql_config --cflags --libs`
gcc gdw_filter.c rw_mysql_config.o gdw3761_filter.o -o gdw_filter -DHHMYSQL -lnetfilter_queue -lmnl `mysql_config --cflags --libs`
gcc dnp_filter.c rw_mysql_config.o dnp_3_0_filter.o -o dnp_filter -DHHMYSQL -lnetfilter_queue -lmnl `mysql_config --cflags --libs`
gcc bacnet_filter.c rw_mysql_config.o bacnet_ip_filter.o -o bacnet_filter -DHHMYSQL -lnetfilter_queue -lmnl `mysql_config --cflags --libs`
gcc ethernet_filter.c rw_mysql_config.o ethernet_ip_filter.o -o ethernet_filter -DHHMYSQL -lnetfilter_queue -lmnl `mysql_config --cflags --libs`

#tar -cPvzf /root/jejer.tar.gz /root/jejer
#tar -cPvzf /root/http.tar.gz /srv/http
#tar -xPvzf /root/jejer.tar.gz
#tar -xPvzf /root/http.tar.gz
set +x