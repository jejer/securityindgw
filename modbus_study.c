#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <arpa/inet.h>

#include <libmnl/libmnl.h>
#include <linux/netfilter.h>
#include <linux/netfilter/nfnetlink.h>

#include <linux/types.h>
#include <linux/netfilter/nfnetlink_queue.h>

#include <libnetfilter_queue/libnetfilter_queue.h>

#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/tcp.h>

#ifdef HHMYSQL
#include <my_global.h>
#include <mysql.h>
#endif

#define MODBUS_TCP_PORT 502
#define MODE_STUDY 0
#define MODE_VERIFY 1
static int MODE = MODE_STUDY;

static unsigned long dest_ip_list[600] = {0};
static struct in6_addr dest_ip6_list[600];

static struct mnl_socket *nl;

static struct nlmsghdr *
nfq_hdr_put(char *buf, int type, uint32_t queue_num)
{
    struct nlmsghdr *nlh = mnl_nlmsg_put_header(buf);
    nlh->nlmsg_type	= (NFNL_SUBSYS_QUEUE << 8) | type;
    nlh->nlmsg_flags = NLM_F_REQUEST;

    struct nfgenmsg *nfg = mnl_nlmsg_put_extra_header(nlh, sizeof(*nfg));
    nfg->nfgen_family = AF_UNSPEC;
    nfg->version = NFNETLINK_V0;
    nfg->res_id = htons(queue_num);

    return nlh;
}

static void
nfq_send_verdict(int queue_num, uint32_t id)
{
    char buf[MNL_SOCKET_BUFFER_SIZE];
    struct nlmsghdr *nlh;

    nlh = nfq_hdr_put(buf, NFQNL_MSG_VERDICT, queue_num);
    nfq_nlmsg_verdict_put(nlh, id, NF_ACCEPT);

    if (mnl_socket_sendto(nl, nlh, nlh->nlmsg_len) < 0)
    {
        perror("mnl_socket_send");
        exit(EXIT_FAILURE);
    }
}

static int queue_cb(const struct nlmsghdr *nlh, void *data)
{
    struct nfqnl_msg_packet_hdr *ph = NULL;
    struct nlattr *attr[NFQA_MAX+1] = {};
    uint32_t id = 0, skbinfo;
    struct nfgenmsg *nfg;
    uint16_t plen;

    if (nfq_nlmsg_parse(nlh, attr) < 0)
    {
        perror("problems parsing");
        return MNL_CB_ERROR;
    }

    nfg = mnl_nlmsg_get_payload(nlh);

    if (attr[NFQA_PACKET_HDR] == NULL)
    {
        fputs("metaheader not set\n", stderr);
        return MNL_CB_ERROR;
    }

    ph = mnl_attr_get_payload(attr[NFQA_PACKET_HDR]);

    plen = mnl_attr_get_payload_len(attr[NFQA_PAYLOAD]);
    void *payload = mnl_attr_get_payload(attr[NFQA_PAYLOAD]);

    struct iphdr *iphdr = (struct iphdr *)payload;
    unsigned long dest_ip = 0;
    struct in6_addr dest_ipv6;
    bool ipv6 = false;

    if (iphdr->version == 4)
    {
        dest_ip = ntohl(iphdr->daddr);
    }
    else
    {
        dest_ipv6 = ((struct ipv6hdr*)iphdr)->daddr;
        ipv6 = true;
    }

    int loop = 0;
    bool found_dest = false;
    int dest_offset = 0;

    //dest loop
    for (loop = 0; loop < 599; loop++)
    {
        if (ipv6)
        {
            if (memcmp(&dest_ip6_list[loop], &dest_ip6_list[599], sizeof(struct in6_addr)) == 0)
            {
                dest_offset = loop;
                break;
            }
            if (memcmp(&dest_ip6_list[loop], &dest_ipv6, sizeof(struct in6_addr)) == 0)
            {
                found_dest = true;
                break;
            }
        }
        else
        {
            if (dest_ip_list[loop] == 0)
            {
                dest_offset = loop;
                break;
            }
            if (dest_ip_list[loop] == dest_ip)
            {
                found_dest = true;
                break;
            }
        }
    }

    if (!found_dest)
    {
        if (MODE == MODE_STUDY)
        {
            // insert to dest_list
            if (ipv6)
            {
                dest_ip6_list[dest_offset] = dest_ipv6;
            }
            else
            {
                dest_ip_list[dest_offset] = dest_ip;
            }
            // insert to database

        }
        else
        {
            // VERIFY FAIL, insert to database

        }
    }

    skbinfo = attr[NFQA_SKB_INFO] ? ntohl(mnl_attr_get_u32(attr[NFQA_SKB_INFO])) : 0;

    if (attr[NFQA_CAP_LEN])
    {
        uint32_t orig_len = ntohl(mnl_attr_get_u32(attr[NFQA_CAP_LEN]));
        if (orig_len != plen)
            printf("truncated ");
    }

    if (skbinfo & NFQA_SKB_GSO)
        printf("GSO ");

    id = ntohl(ph->packet_id);
    //printf("packet received (id=%u hw=0x%04x hook=%u, payload len %u",
    //id, ntohs(ph->hw_protocol), ph->hook, plen);

    /*
     * ip/tcp checksums are not yet valid, e.g. due to GRO/GSO.
     * The application should behave as if the checksums are correct.
     *
     * If these packets are later forwarded/sent out, the checksums will
     * be corrected by kernel/hardware.
     */
    if (skbinfo & NFQA_SKB_CSUMNOTREADY)
        printf(", checksum not ready");
    puts(")");

    nfq_send_verdict(ntohs(nfg->res_id), id);

    return MNL_CB_OK;
}

#ifdef HHMYSQL
static void finish_with_error(MYSQL *con)
{
    fprintf(stderr, "%s\n", mysql_error(con));
    mysql_close(con);
    exit(1);
}

static read_list_from_mysql(unsigned long *dipl, struct in6_addr *dip6l)
{
    MYSQL *con = mysql_init(NULL);

    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() failed\n");
        exit(1);
    }

    if (mysql_real_connect(con, "localhost", "user12", "34klq*",
                           "testdb", 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }

    if (mysql_query(con, "SELECT * FROM Cars"))
    {
        finish_with_error(con);
    }

    MYSQL_RES *result = mysql_store_result(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }

    int num_fields = mysql_num_fields(result);

    MYSQL_ROW row;

    int i = 0;
    while ((row = mysql_fetch_row(result)))
    {
        for(i = 0; i < num_fields; i++)
        {
            printf("%s ", row[i] ? row[i] : "NULL");
        }
        printf("\n");
    }

    mysql_free_result(result);
    mysql_close(con);
};
#endif

int main(int argc, char *argv[])
{
    char *buf;
    /* largest possible packet payload, plus netlink data overhead: */
    size_t sizeof_buf = 0xffff + (MNL_SOCKET_BUFFER_SIZE/2);
    struct nlmsghdr *nlh;
    int ret;
    unsigned int portid, queue_num;

    if (argc != 2)
    {
        printf("Usage: %s [queue_num]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    queue_num = atoi(argv[1]);


    // dest ip list 600
    memset(dest_ip6_list, 0, 600*sizeof(struct in6_addr));

    // get mode and ip list from database
#ifdef HHMYSQL
    read_list_from_mysql(dest_ip_list, dest_ip6_list);
#endif

    nl = mnl_socket_open(NETLINK_NETFILTER);
    if (nl == NULL)
    {
        perror("mnl_socket_open");
        exit(EXIT_FAILURE);
    }

    if (mnl_socket_bind(nl, 0, MNL_SOCKET_AUTOPID) < 0)
    {
        perror("mnl_socket_bind");
        exit(EXIT_FAILURE);
    }
    portid = mnl_socket_get_portid(nl);

    buf = malloc(sizeof_buf);
    if (!buf)
    {
        perror("allocate receive buffer");
        exit(EXIT_FAILURE);
    }

    /* PF_(UN)BIND is not needed with kernels 3.8 and later */
    nlh = nfq_hdr_put(buf, NFQNL_MSG_CONFIG, 0);
    nfq_nlmsg_cfg_put_cmd(nlh, AF_INET, NFQNL_CFG_CMD_PF_UNBIND);

    if (mnl_socket_sendto(nl, nlh, nlh->nlmsg_len) < 0)
    {
        perror("mnl_socket_send");
        exit(EXIT_FAILURE);
    }

    nlh = nfq_hdr_put(buf, NFQNL_MSG_CONFIG, 0);
    nfq_nlmsg_cfg_put_cmd(nlh, AF_INET, NFQNL_CFG_CMD_PF_BIND);

    if (mnl_socket_sendto(nl, nlh, nlh->nlmsg_len) < 0)
    {
        perror("mnl_socket_send");
        exit(EXIT_FAILURE);
    }

    nlh = nfq_hdr_put(buf, NFQNL_MSG_CONFIG, queue_num);
    nfq_nlmsg_cfg_put_cmd(nlh, AF_INET, NFQNL_CFG_CMD_BIND);

    if (mnl_socket_sendto(nl, nlh, nlh->nlmsg_len) < 0)
    {
        perror("mnl_socket_send");
        exit(EXIT_FAILURE);
    }

    nlh = nfq_hdr_put(buf, NFQNL_MSG_CONFIG, queue_num);
    nfq_nlmsg_cfg_put_params(nlh, NFQNL_COPY_PACKET, 0xffff);

    mnl_attr_put_u32(nlh, NFQA_CFG_FLAGS, htonl(NFQA_CFG_F_GSO));
    mnl_attr_put_u32(nlh, NFQA_CFG_MASK, htonl(NFQA_CFG_F_GSO));

    if (mnl_socket_sendto(nl, nlh, nlh->nlmsg_len) < 0)
    {
        perror("mnl_socket_send");
        exit(EXIT_FAILURE);
    }

    /* ENOBUFS is signalled to userspace when packets were lost
     * on kernel side.  In most cases, userspace isn't interested
     * in this information, so turn it off.
     */
    ret = 1;
    mnl_socket_setsockopt(nl, NETLINK_NO_ENOBUFS, &ret, sizeof(int));

    for (;;)
    {
        ret = mnl_socket_recvfrom(nl, buf, sizeof_buf);
        if (ret == -1)
        {
            perror("mnl_socket_recvfrom");
            exit(EXIT_FAILURE);
        }

        ret = mnl_cb_run(buf, ret, 0, portid, queue_cb, NULL);
        if (ret < 0)
        {
            perror("mnl_cb_run");
            exit(EXIT_FAILURE);
        }
    }

    mnl_socket_close(nl);

    return 0;
}
