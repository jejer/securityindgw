#include <my_global.h>
#include <mysql.h>
#include <string.h>

#include "sql_conn_def.h"
#include "filter_config.h"

static MYSQL *con = NULL;

static void finish_with_error(MYSQL *con)
{
    fprintf(stderr, "%s\n", mysql_error(con));
    mysql_close(con);
    exit(1);
}

void init_mysql()
{
    con = mysql_init(NULL);

    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() failed\n");
        exit(1);
    }

    if (mysql_real_connect(con, "localhost", MYSQL_USERNAME, MYSQL_PASSWORD,
                           MYSQL_DATABASE, 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }
}

int read_system_config(int *testmode, int* save_log)
{
    // save log?
    char query_str[255];
    snprintf(query_str, 254, "SELECT value FROM config WHERE name='audit_log_open'");
    if (mysql_query(con, query_str))
    {
        finish_with_error(con);
    }

    MYSQL_RES *result = mysql_store_result(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }

    MYSQL_ROW row = mysql_fetch_row(result);
    if (row == NULL || row[0] == NULL )
    {
        fprintf(stderr, "config get fail.");
        exit(1);
    }

    if (row[0][0] == '0')
    {
        *save_log = 0;
    }
    else
    {
        *save_log = 1;
    }
    mysql_free_result(result);

    // test mode
    snprintf(query_str, 254, "SELECT value FROM config WHERE name='work_mode'");
    if (mysql_query(con, query_str))
    {
        finish_with_error(con);
    }

    result = mysql_store_result(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }

    row = mysql_fetch_row(result);
    if (row == NULL || row[0] == NULL )
    {
        fprintf(stderr, "config get fail.");
        exit(1);
    }

    if (strncmp(row[0], "test", 4) == 0)
    {
        *testmode = 1;
    }
    else
    {
        *testmode = 0;
    }
    mysql_free_result(result);

    return 0;
}

int read_config(int *testmode, int* save_log, int dev_id, int *op_cnt, int *addr_cnt, unsigned char *op_list, struct addr_control *addr_control_list)
{
    memset(op_list, 0, sizeof(unsigned char)*OP_WHITE_LIST_MAX);
    memset(addr_control_list, 0, sizeof(struct addr_control)*ADDR_CONTROL_LIST_MAX);

    init_mysql();

    read_system_config(testmode, save_log);

    // strategy
    char query_str[255];
    snprintf(query_str, 254, "SELECT strategy_id FROM industry_device WHERE uid=%d", dev_id);
    if (mysql_query(con, query_str))
    {
        finish_with_error(con);
    }

    MYSQL_RES *result = mysql_store_result(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }

    MYSQL_ROW row = mysql_fetch_row(result);
    if (row == NULL || row[0] == NULL )
    {
        fprintf(stderr, "strategy_id get fail.");
        exit(1);
    }

    int strategy_id = atoi(row[0]);
    mysql_free_result(result);

    // rule list
    snprintf(query_str, 254, "SELECT rule_id FROM strategy_rule WHERE strategy_id=%d", strategy_id);
    if (mysql_query(con, query_str))
    {
        finish_with_error(con);
    }

    result = mysql_store_result(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }

    int rule_count = 0;
    int rule_list[OP_WHITE_LIST_MAX+ADDR_CONTROL_LIST_MAX];
    while ((row = mysql_fetch_row(result)))
    {
        if (rule_count > OP_WHITE_LIST_MAX+ADDR_CONTROL_LIST_MAX)
        {
            break;
        }

        if (row[0] != NULL)
        {
            rule_list[rule_count] = atoi(row[0]);
            rule_count++;
        }
    }
    mysql_free_result(result);

    // rules
    int i = 0;
    *op_cnt = 0;
    *addr_cnt = 0;
    for (i = 0; i < rule_count; i++)
    {
        snprintf(query_str, 254,
                 "SELECT filter_type,operation_code,register_address,writeable,readable,has_write_lower_limit,has_write_upper_limit,write_lower_limit,write_upper_limit,range_length FROM rule WHERE id=%d",
                 rule_list[i]);

        if (mysql_query(con, query_str))
        {
            finish_with_error(con);
        }

        result = mysql_store_result(con);

        if (result == NULL)
        {
            finish_with_error(con);
        }

        row = mysql_fetch_row(result);
        if (row == NULL || row[0] == NULL )
        {
            fprintf(stderr, "filter_type get fail.");
            exit(1);
        }
        char *filter_type = row[0];

        if (filter_type[0] == '1' && (*op_cnt) < OP_WHITE_LIST_MAX)
        {
            op_list[*op_cnt] = strtol(row[1], NULL, 16);
            (*op_cnt)++;
        }
        else if (filter_type[0] == '2')
        {
            int j = 0;
            while (j < atoi(row[9]) && (*addr_cnt) < ADDR_CONTROL_LIST_MAX)
            {
                addr_control_list[*addr_cnt].addr = strtol(row[2], NULL, 16) + j;
                if (row[3][0] != '0')
                {
                    addr_control_list[*addr_cnt].control |= MODBUS_WRITE;
                }
                if (row[4][0] != '0')
                {
                    addr_control_list[*addr_cnt].control |= MODBUS_READ;
                }
                if (row[5][0] != '0')
                {
                    addr_control_list[*addr_cnt].control |= MODBUS_WRITE_LOW;
                    addr_control_list[*addr_cnt].low = strtol(row[7], NULL, 16);
                }
                if (row[6][0] != '0')
                {
                    addr_control_list[*addr_cnt].control |= MODBUS_WRITE_HIGH;
                    addr_control_list[*addr_cnt].high = strtol(row[8], NULL, 16);
                }

                (*addr_cnt)++;
                j++;
            }
        }

        mysql_free_result(result);
    }

    return 1;
}

int write_result(struct audit_result *result)
{
    const char pattern[] = "INSERT INTO audit_log(time,source_ip,dest,protocol,result,description) VALUES(NOW(),'%s','%s','%s','%s','";
    char tmp_str[512] = {0};
    switch (result->protocol)
    {
    case PROTOCOL_MODBUS:
        snprintf(tmp_str, 511, pattern, result->source, result->dest, "MODBUS", result->result==0?"block":"pass");
        break;
    case PROTOCOL_CJT_188:
        snprintf(tmp_str, 511, pattern, result->source, result->dest, "CJT188", result->result==0?"block":"pass");
        break;
    case PROTOCOL_DLT_645:
        snprintf(tmp_str, 511, pattern, result->source, result->dest, "DLT645", result->result==0?"block":"pass");
        break;
    case PROTOCOL_GDW_3761:
        snprintf(tmp_str, 511, pattern, result->source, result->dest, "GDW376.1", result->result==0?"block":"pass");
        break;
    case PROTOCOL_DNP_3:
        snprintf(tmp_str, 511, pattern, result->source, result->dest, "DNP3", result->result==0?"block":"pass");
        break;
    case PROTOCOL_BACNET:
        snprintf(tmp_str, 511, pattern, result->source, result->dest, "BACNET_IP", result->result==0?"block":"pass");
        break;
    case PROTOCOL_ETHERNET:
        snprintf(tmp_str, 511, pattern, result->source, result->dest, "ETHERNET_IP", result->result==0?"block":"pass");
        break;
    default:
        return;
    }

    int data_max = 60;
    if (result->data_len < data_max)
    {
        data_max = result->data_len;
    }
    const char hex_str[]= "0123456789abcdef";
    char *data = &(tmp_str[strlen(tmp_str)]);
    int i = 0;
    for (i = 0; i < data_max; i++)
    {
        data[i * 2 + 0] = hex_str[result->data[i] >> 4  ];
        data[i * 2 + 1] = hex_str[result->data[i] & 0x0F];
    }

    strncat(tmp_str, "')", 511-strlen(tmp_str));


    if (mysql_query(con, tmp_str))
    {
        return 0;
    }

    return 1;
}

int read_gdw_config(int *testmode, int* save_log, int dev_id, int *op_cnt, GDWFilterType* p_op_list)
{
    memset(p_op_list, 0, sizeof(GDWFilterType)*OP_WHITE_LIST_MAX);

    init_mysql();

    read_system_config(testmode, save_log);

    // strategy
    char query_str[255];
    snprintf(query_str, 254, "SELECT strategy_id FROM industry_device WHERE uid=%d", dev_id);
    if (mysql_query(con, query_str))
    {
        finish_with_error(con);
    }

    MYSQL_RES *result = mysql_store_result(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }

    MYSQL_ROW row = mysql_fetch_row(result);
    if (row == NULL || row[0] == NULL )
    {
        fprintf(stderr, "strategy_id get fail.");
        exit(1);
    }

    int strategy_id = atoi(row[0]);
    mysql_free_result(result);

    // rule list
    snprintf(query_str, 254, "SELECT rule_id FROM strategy_rule WHERE strategy_id=%d", strategy_id);
    if (mysql_query(con, query_str))
    {
        finish_with_error(con);
    }

    result = mysql_store_result(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }

    int rule_count = 0;
    int rule_list[OP_WHITE_LIST_MAX+ADDR_CONTROL_LIST_MAX];
    while ((row = mysql_fetch_row(result)))
    {
        if (rule_count > OP_WHITE_LIST_MAX+ADDR_CONTROL_LIST_MAX)
        {
            break;
        }

        if (row[0] != NULL)
        {
            rule_list[rule_count] = atoi(row[0]);
            rule_count++;
        }
    }
    mysql_free_result(result);

    // rules
    int i = 0;
    *op_cnt = 0;
    for (i = 0; i < rule_count; i++)
    {
        snprintf(query_str, 254,
                 "SELECT filter_type,gdw_afn_b,gdw_afn_e,gdw_pn_b,gdw_pn_e,gdw_fn_b,gdw_fn_e FROM rule WHERE id=%d",
                 rule_list[i]);

        if (mysql_query(con, query_str))
        {
            finish_with_error(con);
        }

        result = mysql_store_result(con);

        if (result == NULL)
        {
            finish_with_error(con);
        }

        row = mysql_fetch_row(result);
        if (row == NULL || row[0] == NULL )
        {
            fprintf(stderr, "rule record get fail.");
            exit(1);
        }

        int filter_type = strtol(row[0], NULL, 10);
        unsigned char gdw_afn_b = strtol(row[1], NULL, 16);
        unsigned char gdw_afn_e = strtol(row[2], NULL, 16);
        unsigned short gdw_pn_b = strtol(row[3], NULL, 16);
        unsigned short gdw_pn_e = strtol(row[4], NULL, 16);
        unsigned short gdw_fn_b = strtol(row[5], NULL, 16);
        unsigned short gdw_fn_e = strtol(row[6], NULL, 16);

        if (filter_type != 10)
        {
            // not GDW rule
            mysql_free_result(result);
            continue;
        }

        if ((*op_cnt) < OP_WHITE_LIST_MAX)
        {
            p_op_list[*op_cnt].AFN_Start = gdw_afn_b;
            p_op_list[*op_cnt].AFN_End = gdw_afn_e;
            p_op_list[*op_cnt].Pn_Start = gdw_pn_b;
            p_op_list[*op_cnt].Pn_End = gdw_pn_e;
            p_op_list[*op_cnt].Fn_Start = gdw_fn_b;
            p_op_list[*op_cnt].Fn_End = gdw_fn_e;
            p_op_list[*op_cnt].PnEnable = row[3][0]=='\0'?0:1;
            p_op_list[*op_cnt].FnEnable = row[5][0]=='\0'?0:1;
            (*op_cnt)++;
        }

        mysql_free_result(result);
    }

    if ((*op_cnt) < OP_WHITE_LIST_MAX)
    {
        p_op_list[*op_cnt].AFN_Start = 0x00;
        p_op_list[*op_cnt].AFN_End = 0x03;
        p_op_list[*op_cnt].PnEnable = 0;
        p_op_list[*op_cnt].FnEnable = 0;
        (*op_cnt)++;
    }
    if ((*op_cnt) < OP_WHITE_LIST_MAX)
    {
        p_op_list[*op_cnt].AFN_Start = 0x06;
        p_op_list[*op_cnt].AFN_End = 0x06;
        p_op_list[*op_cnt].PnEnable = 0;
        p_op_list[*op_cnt].FnEnable = 0;
        (*op_cnt)++;
    }

    return 0;
}

int read_dnp_config(int *testmode, int* save_log, int dev_id, int *op_cnt, DNP3FilterType* p_op_list)
{
    memset(p_op_list, 0, sizeof(DNP3FilterType)*OP_WHITE_LIST_MAX);

    init_mysql();

    read_system_config(testmode, save_log);

    // strategy
    char query_str[255];
    snprintf(query_str, 254, "SELECT strategy_id FROM industry_device WHERE uid=%d", dev_id);
    if (mysql_query(con, query_str))
    {
        finish_with_error(con);
    }

    MYSQL_RES *result = mysql_store_result(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }

    MYSQL_ROW row = mysql_fetch_row(result);
    if (row == NULL || row[0] == NULL )
    {
        fprintf(stderr, "strategy_id get fail.");
        exit(1);
    }

    int strategy_id = atoi(row[0]);
    mysql_free_result(result);

    // rule list
    snprintf(query_str, 254, "SELECT rule_id FROM strategy_rule WHERE strategy_id=%d", strategy_id);
    if (mysql_query(con, query_str))
    {
        finish_with_error(con);
    }

    result = mysql_store_result(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }

    int rule_count = 0;
    int rule_list[OP_WHITE_LIST_MAX+ADDR_CONTROL_LIST_MAX];
    while ((row = mysql_fetch_row(result)))
    {
        if (rule_count > OP_WHITE_LIST_MAX+ADDR_CONTROL_LIST_MAX)
        {
            break;
        }

        if (row[0] != NULL)
        {
            rule_list[rule_count] = atoi(row[0]);
            rule_count++;
        }
    }
    mysql_free_result(result);

    // rules
    int i = 0;
    *op_cnt = 0;
    for (i = 0; i < rule_count; i++)
    {
        snprintf(query_str, 254,
                 "SELECT filter_type,dnp_afn_b,dnp_afn_e,dnp_obj_arr_b,dnp_obj_arr_e,dnp_obj_var_b,dnp_obj_var_e FROM rule WHERE id=%d",
                 rule_list[i]);

        if (mysql_query(con, query_str))
        {
            finish_with_error(con);
        }

        result = mysql_store_result(con);

        if (result == NULL)
        {
            finish_with_error(con);
        }

        row = mysql_fetch_row(result);
        if (row == NULL || row[0] == NULL )
        {
            fprintf(stderr, "rule record get fail.");
            exit(1);
        }

        int filter_type = strtol(row[0], NULL, 10);
        unsigned char dnp_afn_b = strtol(row[1], NULL, 16);
        unsigned char dnp_afn_e = strtol(row[2], NULL, 16);
        unsigned char dnp_group_b = strtol(row[3], NULL, 16);
        unsigned char dnp_group_e = strtol(row[4], NULL, 16);
        unsigned char dnp_variation_b = strtol(row[5], NULL, 16);
        unsigned char dnp_variation_e = strtol(row[6], NULL, 16);

        if (filter_type != 12)
        {
            // not DNP rule
            mysql_free_result(result);
            continue;
        }

        if ((*op_cnt) < OP_WHITE_LIST_MAX)
        {
            p_op_list[*op_cnt].AFN_Start = dnp_afn_b;
            p_op_list[*op_cnt].AFN_End = dnp_afn_e;
            p_op_list[*op_cnt].ObjectGroupNum_Start = dnp_group_b;
            p_op_list[*op_cnt].ObjectGroupNum_End = dnp_group_e;
            p_op_list[*op_cnt].ObjectVariationNum_Start = dnp_variation_b;
            p_op_list[*op_cnt].ObjectVariationNum_End = dnp_variation_e;
            p_op_list[*op_cnt].ObjectGroupEnable = row[3][0]=='\0'?0:1;
            p_op_list[*op_cnt].ObjectVariationEnable = row[5][0]=='\0'?0:1;
            (*op_cnt)++;
        }

        mysql_free_result(result);
    }

    if ((*op_cnt) < OP_WHITE_LIST_MAX)
    {
        p_op_list[*op_cnt].AFN_Start = 0x00;
        p_op_list[*op_cnt].AFN_End = 0x00;
        p_op_list[*op_cnt].ObjectGroupEnable = 0;
        p_op_list[*op_cnt].ObjectVariationEnable = 0;
        (*op_cnt)++;
    }
    if ((*op_cnt) < OP_WHITE_LIST_MAX)
    {
        p_op_list[*op_cnt].AFN_Start = 0x20;
        p_op_list[*op_cnt].AFN_End = 0x21;
        p_op_list[*op_cnt].ObjectGroupEnable = 0;
        p_op_list[*op_cnt].ObjectVariationEnable = 0;
        (*op_cnt)++;
    }
    if ((*op_cnt) < OP_WHITE_LIST_MAX)
    {
        p_op_list[*op_cnt].AFN_Start = 0x81;
        p_op_list[*op_cnt].AFN_End = 0x83;
        p_op_list[*op_cnt].ObjectGroupEnable = 0;
        p_op_list[*op_cnt].ObjectVariationEnable = 0;
        (*op_cnt)++;
    }

    return 0;
}

int read_bacnet_config(int *testmode, int* save_log, int dev_id, int *op_cnt, BACNetIPFilterType* p_op_list)
{
    memset(p_op_list, 0, sizeof(BACNetIPFilterType)*OP_WHITE_LIST_MAX);

    init_mysql();

    read_system_config(testmode, save_log);

    // strategy
    char query_str[255];
    snprintf(query_str, 254, "SELECT strategy_id FROM industry_device WHERE uid=%d", dev_id);
    if (mysql_query(con, query_str))
    {
        finish_with_error(con);
    }

    MYSQL_RES *result = mysql_store_result(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }

    MYSQL_ROW row = mysql_fetch_row(result);
    if (row == NULL || row[0] == NULL )
    {
        fprintf(stderr, "strategy_id get fail.");
        exit(1);
    }

    int strategy_id = atoi(row[0]);
    mysql_free_result(result);

    // rule list
    snprintf(query_str, 254, "SELECT rule_id FROM strategy_rule WHERE strategy_id=%d", strategy_id);
    if (mysql_query(con, query_str))
    {
        finish_with_error(con);
    }

    result = mysql_store_result(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }

    int rule_count = 0;
    int rule_list[OP_WHITE_LIST_MAX+ADDR_CONTROL_LIST_MAX];
    while ((row = mysql_fetch_row(result)))
    {
        if (rule_count > OP_WHITE_LIST_MAX+ADDR_CONTROL_LIST_MAX)
        {
            break;
        }

        if (row[0] != NULL)
        {
            rule_list[rule_count] = atoi(row[0]);
            rule_count++;
        }
    }
    mysql_free_result(result);

    // rules
    int i = 0;
    *op_cnt = 0;
    for (i = 0; i < rule_count; i++)
    {
        snprintf(query_str, 254,
                 "SELECT filter_type,bac_verified,bac_serv_b,bac_serv_e,bac_obj_b,bac_obj_e,bac_sample_b,bac_sample_e FROM rule WHERE id=%d",
                 rule_list[i]);

        if (mysql_query(con, query_str))
        {
            finish_with_error(con);
        }

        result = mysql_store_result(con);

        if (result == NULL)
        {
            finish_with_error(con);
        }

        row = mysql_fetch_row(result);
        if (row == NULL || row[0] == NULL )
        {
            fprintf(stderr, "rule record get fail.");
            exit(1);
        }

        int filter_type = strtol(row[0], NULL, 10);
        int bac_confirmed = strtol(row[1], NULL, 10);
        unsigned char bac_service_b = strtol(row[2], NULL, 16);
        unsigned char bac_service_e = strtol(row[3], NULL, 16);
        unsigned short bac_object_b = strtol(row[4], NULL, 16);
        unsigned short bac_object_e = strtol(row[5], NULL, 16);
        unsigned int bac_instance_b = strtol(row[6], NULL, 16);
        unsigned int bac_instance_e = strtol(row[7], NULL, 16);

        if (filter_type != 9)
        {
            // not BAC rule
            mysql_free_result(result);
            continue;
        }

        if ((*op_cnt) < OP_WHITE_LIST_MAX)
        {
            p_op_list[*op_cnt].ServiceType = bac_confirmed==1?BACNET_IP_FILTER_CONFIRMED_SERVICE:BACNET_IP_FILTER_UNCONFIRMED_SERVICE;
            p_op_list[*op_cnt].ServiceNum_Start = bac_service_b;
            p_op_list[*op_cnt].ServiceNum_End = bac_service_e;
            p_op_list[*op_cnt].ObjectNum_Start = bac_object_b;
            p_op_list[*op_cnt].ObjectNum_End = bac_object_e;
            p_op_list[*op_cnt].InstanceNum_Start = bac_instance_b;
            p_op_list[*op_cnt].InstanceNum_End = bac_instance_e;
            p_op_list[*op_cnt].ObjectEnable = row[4][0]=='\0'?0:1;
            p_op_list[*op_cnt].InstanceEnable = row[6][0]=='\0'?0:1;
            (*op_cnt)++;
        }

        mysql_free_result(result);
    }

    if ((*op_cnt) < OP_WHITE_LIST_MAX)
    {
        p_op_list[*op_cnt].ServiceType = BACNET_IP_FILTER_CONFIRMED_SERVICE;
        p_op_list[*op_cnt].ServiceNum_Start = 0x20;
        p_op_list[*op_cnt].ServiceNum_End = 0x29;
        p_op_list[*op_cnt].ObjectEnable = 0;
        p_op_list[*op_cnt].InstanceEnable = 0;
        (*op_cnt)++;
    }

    if ((*op_cnt) < OP_WHITE_LIST_MAX)
    {
        p_op_list[*op_cnt].ServiceType = BACNET_IP_FILTER_UNCONFIRMED_SERVICE;
        p_op_list[*op_cnt].ServiceNum_Start = 0x07;
        p_op_list[*op_cnt].ServiceNum_End = 0x07;
        p_op_list[*op_cnt].ObjectEnable = 0;
        p_op_list[*op_cnt].InstanceEnable = 0;
        (*op_cnt)++;
    }

    return 0;
}

int read_ethernet_config(int *testmode, int* save_log, int dev_id, int *op_cnt, EthernetIPFilterType* p_op_list)
{
    memset(p_op_list, 0, sizeof(EthernetIPFilterType)*OP_WHITE_LIST_MAX);

    init_mysql();

    read_system_config(testmode, save_log);

    // strategy
    char query_str[255];
    snprintf(query_str, 254, "SELECT strategy_id FROM industry_device WHERE uid=%d", dev_id);
    if (mysql_query(con, query_str))
    {
        finish_with_error(con);
    }

    MYSQL_RES *result = mysql_store_result(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }

    MYSQL_ROW row = mysql_fetch_row(result);
    if (row == NULL || row[0] == NULL )
    {
        fprintf(stderr, "strategy_id get fail.");
        exit(1);
    }

    int strategy_id = atoi(row[0]);
    mysql_free_result(result);

    // rule list
    snprintf(query_str, 254, "SELECT rule_id FROM strategy_rule WHERE strategy_id=%d", strategy_id);
    if (mysql_query(con, query_str))
    {
        finish_with_error(con);
    }

    result = mysql_store_result(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }

    int rule_count = 0;
    int rule_list[OP_WHITE_LIST_MAX+ADDR_CONTROL_LIST_MAX];
    while ((row = mysql_fetch_row(result)))
    {
        if (rule_count > OP_WHITE_LIST_MAX+ADDR_CONTROL_LIST_MAX)
        {
            break;
        }

        if (row[0] != NULL)
        {
            rule_list[rule_count] = atoi(row[0]);
            rule_count++;
        }
    }
    mysql_free_result(result);

    // rules
    int i = 0;
    *op_cnt = 0;
    for (i = 0; i < rule_count; i++)
    {
        snprintf(query_str, 254,
                 "SELECT filter_type,eth_com_b,eth_com_e,eth_type_b,eth_type_e,eth_sample_b,eth_sample_e,eth_property_b,eth_property_e FROM rule WHERE id=%d",
                 rule_list[i]);

        if (mysql_query(con, query_str))
        {
            finish_with_error(con);
        }

        result = mysql_store_result(con);

        if (result == NULL)
        {
            finish_with_error(con);
        }

        row = mysql_fetch_row(result);
        if (row == NULL || row[0] == NULL )
        {
            fprintf(stderr, "rule record get fail.");
            exit(1);
        }

        int filter_type = strtol(row[0], NULL, 10);
        unsigned short eth_cmd_b = strtol(row[1], NULL, 16);
        unsigned short eth_cmd_e = strtol(row[2], NULL, 16);
        unsigned short eth_class_b = strtol(row[3], NULL, 16);
        unsigned short eth_class_e = strtol(row[4], NULL, 16);
        unsigned short eth_instance_b = strtol(row[5], NULL, 16);
        unsigned short eth_instance_e = strtol(row[6], NULL, 16);
        unsigned short eth_attribute_b = strtol(row[7], NULL, 16);
        unsigned short eth_attribute_e = strtol(row[8], NULL, 16);

        if (filter_type != 11)
        {
            // not ETHERNET rule
            mysql_free_result(result);
            continue;
        }

        if ((*op_cnt) < OP_WHITE_LIST_MAX)
        {
            p_op_list[*op_cnt].Command_Start = eth_cmd_b;
            p_op_list[*op_cnt].Command_End = eth_cmd_e;
            p_op_list[*op_cnt].ClassID_Start = eth_class_b;
            p_op_list[*op_cnt].ClassID_End = eth_class_e;
            p_op_list[*op_cnt].Instanse_Start = eth_instance_b;
            p_op_list[*op_cnt].Instanse_End = eth_instance_e;
            p_op_list[*op_cnt].Atrribute_Start = eth_attribute_b;
            p_op_list[*op_cnt].Atrribute_End = eth_attribute_e;
            p_op_list[*op_cnt].ClassIDEnable = row[3][0]=='\0'?0:1;
            p_op_list[*op_cnt].InstanseEnable = row[5][0]=='\0'?0:1;
            p_op_list[*op_cnt].AtrributeEnable = row[7][0]=='\0'?0:1;
            (*op_cnt)++;
        }

        mysql_free_result(result);
    }

    if ((*op_cnt) < OP_WHITE_LIST_MAX)
    {
        p_op_list[*op_cnt].Command_Start = 0x0000;
        p_op_list[*op_cnt].Command_End = 0x0000;
        p_op_list[*op_cnt].ClassIDEnable = 0;
        p_op_list[*op_cnt].InstanseEnable = 0;
        p_op_list[*op_cnt].AtrributeEnable = 0;
        (*op_cnt)++;
    }
    if ((*op_cnt) < OP_WHITE_LIST_MAX)
    {
        p_op_list[*op_cnt].Command_Start = 0x0064;
        p_op_list[*op_cnt].Command_End = 0x0066;
        p_op_list[*op_cnt].ClassIDEnable = 0;
        p_op_list[*op_cnt].InstanseEnable = 0;
        p_op_list[*op_cnt].AtrributeEnable = 0;
        (*op_cnt)++;
    }
    if ((*op_cnt) < OP_WHITE_LIST_MAX)
    {
        p_op_list[*op_cnt].Command_Start = 0x0073;
        p_op_list[*op_cnt].Command_End = 0x0073;
        p_op_list[*op_cnt].ClassIDEnable = 0;
        p_op_list[*op_cnt].InstanseEnable = 0;
        p_op_list[*op_cnt].AtrributeEnable = 0;
        (*op_cnt)++;
    }

    return 0;
}

int write_dos_alarm(const char *source_ip_str)
{
    const char pattern[] = "INSERT INTO log(created_time,operate_type,level,operate_detail,ip) VALUES(NOW(),53,2,'DoS Attack','%s')";
    char tmp_str[512] = {0};
    snprintf(tmp_str, 511, pattern, source_ip_str);

    if (mysql_query(con, tmp_str))
    {
        return 0;
    }

    return 1;
}
