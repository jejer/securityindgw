#ifndef _GDW3761_FILTER_H
#define _GDW3761_FILTER_H

typedef struct GDWFilterType_st
{
    //第一级AFN过滤
    unsigned char	AFN_Start;
    unsigned char	AFN_End; 

    //第二级Pn过滤
    unsigned char	PnEnable;	//是否启用
    unsigned short	Pn_Start;
    unsigned short	Pn_End;

    //第三级Fn过滤
    unsigned char	FnEnable;	//是否启用，在第二级启用的前提下
    unsigned short	Fn_Start;
    unsigned short	Fn_End;
} GDWFilterType;

/***************************************************************************************************
*\Function      GDWFilterInit
*\Description   国网规约协议过滤初始化
*\Parameter     filter_list 协议过滤规则列表，类型为GDWFilterAFNType类型的数组
*\Parameter     num         协议过滤规则数目
*\Return        int         是否初始化成功，0：成功，1：失败
*\Note          
*               创建函数。
***************************************************************************************************/
int GDWFilterInit(GDWFilterType* filter_list, int num);

/***************************************************************************************************
*\Function      EthernetIPFilterCheck
*\Description   EthernetIP协议数据包过滤检查
*\Parameter     ip_packet_buf           IP层数据包
*\Parameter     ip_packet_buf_length    IP数据包长度
*\Return        int                     该数据包是否可通过,0:通过，1：不能通过
*\Note          
***************************************************************************************************/
int GDWFilterCheck(char* ip_packet_buf, int ip_packet_buf_length);

#endif /*_GDW3761_FILTER_H*/
