#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>

#include <unistd.h>
#include <stdlib.h>

#include <getopt.h>
#include <string.h>

#include "DLT.h"
#include <fcntl.h>
#include <termios.h>

#include <arpa/inet.h>

#include "filter_config.h"


static unsigned char op_white_list[OP_WHITE_LIST_MAX] = {0};
static int op_cnt = 0;
static struct addr_control addr_control_list[ADDR_CONTROL_LIST_MAX];
static int addr_cnt = 0;
static int testmode = 0;
static int save_log = 0;
static struct audit_result mysql_result;


#define BUFF_SIZE 2048
static unsigned char send_buff[BUFF_SIZE+3];
static unsigned char recv_buff[BUFF_SIZE];

static int do_serial_operation(int fd, int len);
static int dlt_filter(unsigned char* mesg, int data_len, int* p_offset);

int openport(char *Dev)
{
    int fd = open( Dev, O_RDWR|O_NOCTTY|O_NDELAY );
    if (-1 == fd)
    {
        perror("Can''t Open Serial Port");
        return -1;
    }
    else
        return fd;
}

int readport(int fd, unsigned char *buf,int len,int maxwaittime)
{
    struct timeval tv;
    fd_set readfd;
    tv.tv_sec=maxwaittime/1000;    //SECOND
    tv.tv_usec=maxwaittime%1000*1000;  //USECOND
    FD_ZERO(&readfd);
    FD_SET(fd,&readfd);

    int read_cnt = 0;
    int try_cnt = 0;
    int rc = 0;

    do
    {
        rc=select(fd+1,&readfd,NULL,NULL,&tv);
        if (rc>0)  // have read event
        {
            rc=read(fd,&buf[read_cnt],1);
            if (rc>0)   // read success
            {
                read_cnt++;
            }
        }
        else
        {
            return -1;
        }
        try_cnt++;
    }
    while (read_cnt < len && try_cnt < len);

    return read_cnt;
}

void writeport(int fd, unsigned char *buf, int len)
{
    write(fd,buf,len);
}

void flushport(int fd)
{
    tcflush(fd,TCIOFLUSH);
}

void bin_to_strhex(unsigned char *bin, unsigned int binsz)
{
    if (!binsz)
        return;

    char          hex_str[]= "0123456789abcdef";
    unsigned int  i;

    char *result = (char *)malloc(binsz * 2 + 1);
    (result)[binsz * 2] = 0;

    for (i = 0; i < binsz; i++)
    {
        (result)[i * 2 + 0] = hex_str[bin[i] >> 4  ];
        (result)[i * 2 + 1] = hex_str[bin[i] & 0x0F];
    }
    printf(result);
    free(result);
}

int main(int argc, char**argv)
{
    char tty_device[255] = {0};
    short udp_port = 0;
    int opt;
    int dev_id = 0;
    while ((opt = getopt(argc, argv, "p:d:")) != -1)
    {
        switch (opt)
        {
        case 'p':
            udp_port = atoi(optarg);
            dev_id = udp_port - 5000;
            break;
        case 'd':
            strncpy(tty_device, optarg, 254);
            break;
        default: /* '?' */
            fprintf(stderr, "Usage: %s -p udp port -d serial device\n",
                    argv[0]);
            exit(-1);
        }
    }

    if (strlen(tty_device) == 0)
    {
        fprintf(stderr, "Usage: %s -p udp port -d serial device\n",
                argv[0]);
        exit(-1);
    }

    int serialfd = openport(tty_device);
    if (serialfd <= 0)
    {
        fprintf(stderr, "Cannot open tty device %s", tty_device);
        exit(-1);
    }
    flushport(serialfd);

    int sockfd,n;
    struct sockaddr_in servaddr,cliaddr;
    socklen_t len;
    unsigned char mesg[1000];

    sockfd=socket(AF_INET,SOCK_DGRAM,0);

    bzero(&servaddr,sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr=htonl(INADDR_ANY);
    servaddr.sin_port=htons(udp_port);
    bind(sockfd,(struct sockaddr *)&servaddr,sizeof(servaddr));

    // read config
    //init_mysql();
    read_config(&testmode, &save_log, dev_id, &op_cnt, &addr_cnt, op_white_list, &(addr_control_list[0]));
    mysql_result.protocol = PROTOCOL_DLT_645;
    strcpy(mysql_result.dest, tty_device);
    send_buff[0] = 0xFE;
    send_buff[1] = 0xFE;
    send_buff[2] = 0xFE;

    for (;;)
    {
        len = sizeof(cliaddr);
        //sleep(1);
        n = recvfrom(sockfd,mesg,1000,0,(struct sockaddr *)&cliaddr,&len);
        //sendto(sockfd,mesg,n,0,(struct sockaddr *)&cliaddr,sizeof(cliaddr));
        printf("-------------------------------------------------------\n");
        //mesg[n] = 0;
        printf("Received the following:\n");
        //printf("%s",mesg);
        bin_to_strhex((unsigned char*)mesg, n);
        printf("-------------------------------------------------------\n");

        if (n < sizeof(struct DLT_645))
        {
            continue;
        }

        mysql_result.data = (unsigned char*)mesg;
        mysql_result.data_len = n;
        strcpy(mysql_result.source, inet_ntoa(cliaddr.sin_addr));

        int offset = 0;
        int block = dlt_filter(mesg, n, &offset);
        int pass = block?0:1;

        mysql_result.result = pass;
        if (save_log != 0)
        {
            if (pass == 0 || testmode == 1)
            {
                write_result(&mysql_result);
            }
        }
        if (pass == 0)
        {
            continue;
        }

        memcpy(send_buff+3, mesg+offset, n>BUFF_SIZE?BUFF_SIZE:n);
        n = do_serial_operation(serialfd, (n>BUFF_SIZE?BUFF_SIZE:n)+3);
        sendto(sockfd,recv_buff,n,0,(struct sockaddr *)&cliaddr,sizeof(cliaddr));
    }
}

int do_serial_operation(int fd, int len)
{
    flushport(fd);
    writeport(fd, send_buff, len);
    memset(recv_buff, 0, BUFF_SIZE);
    int retry = 0;
    int rc = 0;
    do
    {
        retry++;
        rc = readport(fd, recv_buff, 1, 500);
        if (rc == 1 && recv_buff[0] == DLT_START)
        {
            break;
        }
    }
    while (retry < 6);

    if (recv_buff[0] != DLT_START)
    {
        flushport(fd);
        return 0;
    }

    rc = readport(fd, recv_buff+1 , sizeof(struct DLT_645) -1 , 500);
    if (rc != (sizeof(struct DLT_645) -1))
    {
        flushport(fd);
        return 0;
    }

    struct DLT_645 *dlthdr = (struct DLT_645 *)recv_buff;
    if (sizeof(struct DLT_645) + dlthdr->data_len + 2 > BUFF_SIZE)
    {
        flushport(fd);
        return 0;
    }
    rc = readport(fd, recv_buff+sizeof(struct DLT_645) , dlthdr->data_len + 2 , 500);
    if (rc != (dlthdr->data_len + 2))
    {
        flushport(fd);
        return 0;
    }

    unsigned char checksum = *(((unsigned char*)dlthdr)+sizeof(struct DLT_645)+dlthdr->data_len);
    int i = 0;
    unsigned char checkcalc = 0;
    for (i = 0; i < sizeof(struct DLT_645)+dlthdr->data_len; i++)
    {
        checkcalc += *((unsigned char*)dlthdr + i);
    }
    if (checksum != checkcalc)
    {
        return 0;
    }

    return sizeof(struct DLT_645) + dlthdr->data_len + 2;
}

// 0-pass 1-block
int dlt_filter(unsigned char* mesg, int data_len, int* p_offset)
{
    int i = 0;
    for (i = 0; i < 6; i++)
    {
        if (mesg[i] == DLT_START)
        {
            break;
        }
    }
    if (mesg[i] != DLT_START)
    {
        return 1;
    }
    *p_offset = i;

    struct DLT_645 *hdr = (struct DLT_645*)(mesg + *p_offset);
    // verify check sum
    unsigned char checksum = *(((unsigned char*)hdr)+sizeof(struct DLT_645)+hdr->data_len);
    unsigned char checkcalc = 0;
    for (i = 0; i < sizeof(struct DLT_645)+hdr->data_len; i++)
    {
        checkcalc += *((unsigned char*)hdr + i);
    }
    if (checksum != checkcalc)
    {
        return 1;
    }


    int pass = 0;
    for (i = 0; i < op_cnt; i++)
    {
        if (hdr->control_code == op_white_list[i])
        {
            pass = 1;
            break;
        }
    }

    if (pass == 0)
    {
        return 1;
    }

    struct DLT_645_WITH_DI *di_hdr = (struct DLT_645_WITH_DI *)hdr;
    unsigned short di = di_hdr->DI1;
    di = ((di << 8)&0xFF00) | di_hdr->DI0;
    pass = 0;
    for (i = 0; i < addr_cnt && pass == 0; i++)
    {
        struct addr_control *addr_filter = &addr_control_list[i];

        if (addr_filter->addr == di)
        {
            pass = 1;
            break;
        }
    }

    if (pass == 0)
    {
        return 1;
    }

    return 0;
}
