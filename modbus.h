#ifndef MODBUS_H_INCLUDED
#define MODBUS_H_INCLUDED

#pragma pack(push, 1)
struct modbus_pdu {
    unsigned char op_code;
    unsigned char data[0];
};

struct modbus_rtu {
    unsigned char addr;
    struct modbus_pdu pdu;
};

struct modbus_adu {
    unsigned short tid;
    unsigned short pid;
    unsigned short len;
    unsigned char uid;
    struct modbus_pdu pdu;
};

/* �����ּĴ���(03) ������Ĵ���(04) */
struct op_03_04{
    unsigned short start_addr;  /* 0x0000 - 0xFFFF */
    unsigned short reg_count;   /* 1-125(0x7D) */
};

/* д�����Ĵ��� */
struct op_06{
    unsigned short addr; /* 0x0000 - 0xFFFF */
    unsigned short val;  /* 0x0000 - 0xFFFF */
};

/* д����Ĵ��� */
struct op_10{
    unsigned short start_addr; /* 0x0000 - 0xFFFF */
    unsigned short reg_count;  /* 0x0000 - 0x0078 */
    unsigned char data_len;    /* reg_count*2 */
    unsigned short data[0];    /* reg_count*2 byte */
};

/* ��д����Ĵ��� */
struct op_17{
    unsigned short read_start_addr;  /* 0x0000 - 0xFFFF */
    unsigned short read_reg_count;   /* 0x0001 - 0x0076 */
    unsigned short write_start_addr; /* 0x0000 - 0xFFFF */
    unsigned short write_reg_count;  /* 0x0001 - 0x0076 */
    unsigned char write_data_len;    /* write_reg_count*2 */
    unsigned short write_data[0];    /* write_reg_count*2 byte */
};
#pragma pack(pop)

#endif // MODBUS_H_INCLUDED
