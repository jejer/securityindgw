#ifndef _DNP_3_0_FILTER_H
#define _DNP_3_0_FILTER_H

typedef struct DNPFilterType_st
{
    //第一级，AFN过滤
    unsigned char  AFN_Start;
    unsigned char  AFN_End;

    //第二级，Object Group过滤
    unsigned char ObjectGroupEnable;//是否启用，在AFN值属于03H、04H、05H、06H的前提下
    unsigned char ObjectGroupNum_Start;
    unsigned char ObjectGroupNum_End;

    //第三级，Object variation过滤
    unsigned char ObjectVariationEnable;//是否启用，在第二级启用的前提下
    unsigned char ObjectVariationNum_Start;
    unsigned char ObjectVariationNum_End;
}DNP3FilterType;

/***************************************************************************************************
*\Function      DNP3FilterInit
*\Description   DNP3.0协议过滤初始化
*\Parameter     filter_list 协议过滤规则列表，类型为DNPFilterAFNType类型的数组
*\Parameter     num         协议过滤规则数目
*\Return        int         是否初始化成功，0：成功，1：失败
*\Note          
***************************************************************************************************/
int DNP3FilterInit(DNP3FilterType* filter_list, int num);

/***************************************************************************************************
*\Function      DNP3FilterCheck
*\Description   DNP3.0协议数据包过滤检查
*\Parameter     ip_packet_buf           IP层数据包
*\Parameter     ip_packet_buf_length    IP数据包长度
*\Return        int                     该数据包是否可通过,0:通过，1：不能通过
*\Note          
***************************************************************************************************/
int DNP3FilterCheck(char* ip_packet_buf, int ip_packet_buf_length);

#endif /*_DNP_3_0_FILTER_H*/
