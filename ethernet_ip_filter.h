#ifndef _ETHERNET_IP_FILTER_H
#define _ETHERNET_IP_FILTER_H

typedef struct EthernetIPFilterType_st
{
    //第一级，命令编号过滤
    unsigned short	Command_Start;				
    unsigned short	Command_End;

    //第二级，ClassID过滤
    unsigned char 	ClassIDEnable;//是否启用
    unsigned short	ClassID_Start; 
    unsigned short	ClassID_End; 

    //第三级，Instanse过滤
    unsigned char 	InstanseEnable;//是否启用，在第二级启用的前提下
    unsigned short	Instanse_Start;
    unsigned short	Instanse_End;

    //第四级，Atrribute过滤
    unsigned char 	AtrributeEnable; //是否启用，在第三级启用的前提下
    unsigned short	Atrribute_Start;
    unsigned short	Atrribute_End;
} EthernetIPFilterType;

/***************************************************************************************************
*\Function      EthernetIPFilterInit
*\Description   EthernetIP协议过滤初始化
*\Parameter     filter_list 协议过滤规则列表，类型为EthernetIPFilterCommandType类型的数组
*\Parameter     num         协议过滤规则数目
*\Return        int         是否初始化成功，0：成功，1：失败
*\Note          
***************************************************************************************************/
int EthernetIPFilterInit(EthernetIPFilterType * filter_list, int num);

/***************************************************************************************************
*\Function      EthernetIPFilterCheck
*\Description   EthernetIP协议数据包过滤检查
*\Parameter     ip_packet_buf           IP层数据包
*\Parameter     ip_packet_buf_length    IP数据包长度
*\Return        int                     该数据包是否可通过,0:通过，1：不能通过
*\Note          
***************************************************************************************************/
int EthernetIPFilterCheck(char* ip_packet_buf, int ip_packet_buf_length);

#endif /*_ETHERNET_IP_FILTER_H*/
