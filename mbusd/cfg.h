#ifndef _CFG_H
#define _CFG_H

#include "globals.h"
#include "log.h"
#include "tty.h"
#include "conn.h"

/* Global configuration storage structure */
typedef struct
{
#ifdef LOG
  /* debug level */
  char dbglvl;
  /* log file name */
  char logname[INTBUFSIZE + 1];
#endif
  /* tty port name */
  char ttyport[INTBUFSIZE + 1];
  /* tty speed */
  int ttyspeed;
  /* trx control type (0 - ADDC, 1 - by RTS) */
  int trxcntl;
  /* TCP server port number */
  int serverport;
  /* maximum number of connections */
  int maxconn;
  /* number of tries of request in case timeout (0 - no tries attempted) */
  int maxtry;
  /* staled connection timeout (in sec) */
  int conntimeout;
  /* inter-request pause (in msec) */
  unsigned long rqstpause;
  /* response waiting time (in msec) */
  unsigned long respwait;
  /* inter-byte response pause (in usec) */
  unsigned long resppause;
} cfg_t;

/* Prototypes */
extern cfg_t cfg;
void cfg_init(void);

#endif /* _CFG_H */ 
