#ifndef _LOG_H
#define _LOG_H

#include "globals.h"

extern int isdaemon;

/* Default log file path and name */
#define LOGPATH "/var/log/"
#define LOGNAME "mbus.log"

#ifdef LOG
int log_init(char *logname);
int log_app(char *logname, char *string);
void log(int level, char *fmt, ...);
#endif

#endif /* _LOG_H */
