#ifndef _CRC_H
#define _CRC_H

#include "globals.h"

unsigned short crc16(unsigned char *buf, unsigned int bsize);

#endif /*_CRC_H */
