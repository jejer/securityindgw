#include "cfg.h"

/* Global configuration storage variable */
cfg_t cfg;

/*
 * Setting up config defaults
 */
void 
cfg_init(void)
{
#ifdef LOG
  cfg.dbglvl = 2;
  strncpy(cfg.logname, LOGNAME, INTBUFSIZE);
#endif
  strncpy(cfg.ttyport, DEFAULT_PORT, INTBUFSIZE);
  cfg.ttyspeed = DEFAULT_SPEED;
#ifdef  TRXCTL
  cfg.trxcntl = TRX_ADDC;
#endif
  cfg.serverport = DEFAULT_SERVERPORT;
  cfg.maxconn = DEFAULT_MAXCONN;
  cfg.maxtry = DEFAULT_MAXTRY;
  cfg.rqstpause = DEFAULT_RQSTPAUSE;
  cfg.respwait = DEFAULT_RESPWAIT;
  cfg.resppause = DV(3, cfg.ttyspeed);
  cfg.conntimeout = DEFAULT_CONNTIMEOUT;
}
