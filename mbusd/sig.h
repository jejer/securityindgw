#ifndef _SIG_H
#define _SIG_H

#include "globals.h"
#ifdef LOG
#  include "log.h"
#endif

/* prototypes */
extern volatile sig_atomic_t sig_flag;
void sig_init(void);
void sig_exec(void);

#endif /* _SIG_H */
