#ifndef _SOCKUTILS_H
#define _SOCKUTILS_H

#include "globals.h"
#ifdef LOG
#  include "log.h"
#endif

#define BACKLOG 5

/* Socket buffers size */
#define SOCKBUFSIZE 512

int sock_set_blkmode(int sd, int blkmode);
int sock_create(int blkmode);
int sock_create_server(char *server_ip,
  unsigned short server_port, int blkmode);
int sock_accept(int server_sd,
  struct sockaddr_in *rmt_addr, int blkmode);

#endif
