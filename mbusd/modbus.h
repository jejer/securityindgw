#ifndef _MODBUS_H
#define _MODBUS_H

#include "globals.h"
#include "crc16.h"

/*
 * Macros for invoking data from MODBUS packet header
 */
#define MB_HDR(p, d) ( *(p + d) )

/*
 * MODBUS frame lengths
 */
#define MB_EX_LEN  3
#define	MB_MIN_LEN 4
#define	MB_MAX_LEN 256

/*
 * Macroses for word operations
 */
#define HIGH(w) ( (unsigned char)(((w) >> 8) & 0xff) )
#define LOW(w) ( (unsigned char)((w) & 0xff) )
#define WORD_WR_BE(p, w) do \
                         { *(p) = (unsigned char)(((w) >> 8) & 0xff); \
                           *(p + 1) = (unsigned char)((w) & 0xff); } \
                         while (0);
#define WORD_RD_BE(p) ( (((unsigned short)*(p) << 8) & 0xff00) + \
                        (*(p + 1) & 0xff) )
#define WORD_WR_LE(p, w) do \
                         {  *(p) = (unsigned char)((w) & 0xff); \
                            *(p + 1) = (unsigned char)(((w) >> 8) & 0xff); } \
                         while (0);
#define WORD_RD_LE(p) ( (((unsigned short)*(p + 1) << 8) & 0xff00) + \
                        (*(p) & 0xff) )

/*
 * MODBUS packet structure
 */
#define MB_TRANS_ID_H 0    /* transaction ID high byte */
#define MB_TRANS_ID_L 1    /* transaction ID low byte */
#define MB_PROTO_ID_H 2    /* protocol ID high byte */
#define MB_PROTO_ID_L 3    /* protocol ID low byte */
#define MB_LENGTH_H   4    /* length field high byte */
#define MB_LENGTH_L   5    /* length field low byte */
#define MB_UNIT_ID    6    /* unit identifier */
#define MB_FCODE      7    /* function code */
#define MB_DATA       8    /* MODBUS data */
                      
/*
 * Exception codes
 */
#define MB_EX_CRC     128
#define MB_EX_TIMEOUT 129

/* Prototypes */
int modbus_crc_correct(unsigned char *frame, unsigned int len);
void modbus_crc_write(unsigned char *frame, unsigned int len);
void modbus_ex_write(unsigned char *packet, unsigned char code);
int modbus_check_header(unsigned char *packet);


#endif /* _MODBUS_H */
