#include "modbus.h"

/*
 * Check CRC of MODBUS frame
 * Parameters: FRAME - address of the frame,
 *             LEN   - frame length;
 * Return: 0 if CRC failed,
 *         non-zero otherwise
 */
int
modbus_crc_correct(unsigned char *frame, unsigned int len)
{
  return (!crc16(frame, len));
}

/*
 * Write CRC to MODBUS frame
 * Parameters: FRAME - address of the frame,
 *             LEN   - frame length;
 * Return: none
 */
void
modbus_crc_write(unsigned char *frame, unsigned int len)
{
  WORD_WR_LE(frame + len, crc16(frame, len));
}

/*
 * Write exception response to OpenMODBUS request
 * Parameters: PACKET - address of the request packet,
 *             CODE - exception code;
 * Return: none
 */
void
modbus_ex_write(unsigned char *packet, unsigned char code)
{
  MB_HDR(packet, MB_FCODE) |= 0x80;
  MB_HDR(packet, MB_DATA) = code;
  WORD_WR_BE(packet + MB_LENGTH_H, MB_EX_LEN);
}

/*
 * Check MODBUS packet header consistency
 * Parameters: HEADER - address of the header
 * Return: RC_OK if (mostly) all is right, RC_ERR otherwise
 */
int
modbus_check_header(unsigned char *packet)
{
  return (MB_HDR(packet, MB_PROTO_ID_H) == 0 &&
          MB_HDR(packet, MB_PROTO_ID_L) == 0 &&   
          MB_HDR(packet, MB_LENGTH_H) == 0   &&
          MB_HDR(packet, MB_LENGTH_L) > 0) ? RC_OK : RC_ERR;
}
