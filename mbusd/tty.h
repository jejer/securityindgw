#ifndef _TTY_H
#define _TTY_H

#include "globals.h"
#include "cfg.h"

/*
 * Delay value calculation macros
 */
#define	DV(x, y) (x * 10000000l / y)

/*
 * Default tty port parameters
 */
#if defined (__CYGWIN__)
#  define DEFAULT_PORT "/dev/COM1"
#elif defined (__linux__)
#  define DEFAULT_PORT "/dev/ttyS0"
#else
#  define DEFAULT_PORT "/dev/cuaa0"
#endif

#define DEFAULT_SPEED 19200
#define DEFAULT_BSPEED B19200

/*
 * Maximum tty buffer size
 */
#define TTY_BUFSIZE 256

/*
 * TRX control types
 */
#ifdef  TRXCTL
#  define TRX_ADDC 0
#  define TRX_RTS  !TRX_ADDC
#endif

/*
 * TTY device FSM states
 */
#define TTY_PAUSE 0
#define TTY_READY 1
#define TTY_RQST  2
#define TTY_RESP  3

/*
 * TTY related data storage structure
 */
typedef struct
{
  int fd;                       /* tty file descriptor */
  int speed;                    /* serial port speed */
  char *port;                   /* serial port device name */
#ifdef TRXCTL
  int trxcntl;                  /* trx control type (0 - ADDC, RTS otherwise) */
#endif
  struct termios tios;          /* working termios structure */
  struct termios savedtios;     /* saved termios structure */
  int state;                    /* current state */
  unsigned int trynum;             /* try counter */
  unsigned long timer;          /* time tracking variable */
  unsigned int txlen;           /* tx data length */
  unsigned int rxlen;           /* rx data length */
  unsigned char ptrbuf;         /* ptr in the buffer */
  unsigned char txbuf[TTY_BUFSIZE]; /* transmitting buffer */
  unsigned char rxbuf[TTY_BUFSIZE]; /* receiving buffer */
} ttydata_t;

/* prototypes */
void tty_sighup(void);
#ifdef TRXCTL
void tty_init(ttydata_t *mod, char *port, int speed, int trxcntl);
#else
void tty_init(ttydata_t *mod, char *port, int speed);
#endif
int tty_open(ttydata_t *mod);
int tty_set_attr(ttydata_t *mod);
speed_t tty_transpeed(int speed);
int tty_cooked(ttydata_t *mod);
int tty_close(ttydata_t *mod);
void tty_set_rts(int fd);
void tty_clr_rts(int fd);
void tty_delay(int usec);

#endif /* _TTY_H */
