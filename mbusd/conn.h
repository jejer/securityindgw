#ifndef _CONN_H
#define _CONN_H

#include "globals.h"
#include "cfg.h"
#include "sock.h"
#include "modbus.h"
#include "sig.h"
#ifdef LOG
#include "log.h"
#endif

/*
 * Default values
 */
#define DEFAULT_SERVERPORT 502
#define DEFAULT_MAXCONN 32
#define DEFAULT_MAXTRY 3
#define DEFAULT_RQSTPAUSE 100
#define DEFAULT_RESPWAIT 50
#define DEFAULT_CONNTIMEOUT 60

#define CRCSIZE 2       /* size (in bytes) of CRC */
#define HDRSIZE 6       /* size (in bytes) of header */
#define BUFSIZE 256     /* size (in bytes) of MODBUS data */
#define RQSTSIZE (HDRSIZE + BUFSIZE - 2) /* size (in bytes) of MODBUS request */

/*
 * Client connection FSM states
 */
#define CONN_HEADER 0
#define CONN_RQST   1
#define CONN_TTY    2
#define CONN_RESP   3

/*
 * Client connection related data storage structure
 */
typedef struct conn_t
{
  struct conn_t *prev;  /* linked list previous connection */
  struct conn_t *next;  /* linked list next connection */
  int sd;               /* socket descriptor */
  int state;            /* current state */
  int timeout;          /* timeout value, secs */
  struct sockaddr_in sockaddr; /* connection structure */
  int ctr;              /* counter of data in the buffer */
  unsigned char buf[HDRSIZE + BUFSIZE];    /* data buffer */
} conn_t;

/* prototypes */
int conn_init(void);
void conn_loop(void);
void conn_open(void);
conn_t *conn_close(conn_t *conn);

#endif /* _CONN_H */
