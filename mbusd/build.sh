gcc -c cfg.c -o cfg.o -g
gcc -c conn.c -o conn.o -g
gcc -c crc16.c -o crc16.o -g
gcc -c log.c -o log.o -g
gcc -c modbus.c -o modbus.o -g
gcc -c queue.c -o queue.o -g
gcc -c sig.c -o sig.o -g
gcc -c sock.c -o sock.o -g
gcc -c state.c -o state.o -g
gcc -c tty.c -o tty.o -g
gcc main.c cfg.o conn.o crc16.o log.o modbus.o queue.o sig.o sock.o state.o tty.o -o mbusd -g -DPACKAGE=\"mbusd\" -DVERSION=\"0.1.3\"