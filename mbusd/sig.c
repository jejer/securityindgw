#include "sig.h"
#include "tty.h"

/* signal flag */
volatile sig_atomic_t sig_flag = 0;

/* internal prototypes */
void sig_bus(int signum);
void sig_segv(int signum);
void sig_handler(int signum);

/*
 * Signal handler initialization
 * Return: none
 */
void
sig_init(void)
{
  struct sigaction sa;
  
  sa.sa_flags = 0;

  sigemptyset(&sa.sa_mask);
  sigaddset(&sa.sa_mask, SIGSEGV);
  sigaddset(&sa.sa_mask, SIGBUS);
  sigaddset(&sa.sa_mask, SIGTERM);
  sigaddset(&sa.sa_mask, SIGHUP);
  sigaddset(&sa.sa_mask, SIGINT);
  sigaddset(&sa.sa_mask, SIGPIPE);
  sigaddset(&sa.sa_mask, SIGCHLD);
  sigaddset(&sa.sa_mask, SIGALRM);
  sigaddset(&sa.sa_mask, SIGUSR1);
  sigaddset(&sa.sa_mask, SIGUSR2);

  sa.sa_handler = sig_segv;
  sigaction(SIGSEGV, &sa, NULL);

  sa.sa_handler = sig_bus;
  sigaction(SIGBUS, &sa, NULL);

  sa.sa_handler = sig_handler;
  sigaction(SIGTERM, &sa, NULL);

  sa.sa_handler = sig_handler;
  sigaction(SIGHUP, &sa, NULL);

  sa.sa_handler = sig_handler;
  sigaction(SIGINT, &sa, NULL);

  sa.sa_handler = SIG_IGN;
  sigaction(SIGPIPE, &sa, NULL);

  sa.sa_handler = SIG_IGN;
  sigaction(SIGCHLD, &sa, NULL);

  sa.sa_handler = SIG_IGN;
  sigaction(SIGALRM, &sa, NULL);

  sa.sa_handler = SIG_IGN;
  sigaction(SIGUSR1, &sa, NULL);

  sa.sa_handler = SIG_IGN;
  sigaction(SIGUSR2, &sa, NULL);
}

/*
 * SIGSEGV signal handler
 */
void
sig_segv(int signum)
{
#ifndef SAFESIG
  signum = signum; /* prevent compiler warning */
  fprintf(stderr, "caught SIGSEGV, dumping core...");
  fclose(stderr);
#endif
  abort();
}

/*
 * SIGBUS signal handler
 */
void
sig_bus(int signum)
{
#ifndef SAFESIG
  signum = signum; /* prevent compiler warning */
  fprintf(stderr, "caught SIGBUS, dumping core...");
  fclose(stderr);
#endif
  abort();
}

/*
 * Unignored signals handler
 */
void
sig_handler(int signum)
{
#ifndef SAFESIG
  tty_sighup();
#endif
  sig_flag = signum;
}

/*
 * Signal action execution
 */
void
sig_exec(void)
{
#ifdef LOG
  static char *signames[] = { 
    "", "HUP", "INT", "QUIT", "ILL", "TRAP", "IOT", "BUS", "FPE",
    "KILL", "USR1", "SEGV", "USR2", "PIPE", "ALRM", "TERM" };
  log(2, "Terminated by signal: SIG%s", signames[sig_flag]);
#endif
  /* currently simply exit the program */
  exit(1);
}
