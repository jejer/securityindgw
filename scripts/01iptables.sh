#!/bin/sh

set -x

MYSQL_DB="gateway"
MYSQL_USER="gateway"
MYSQL_PASS="gateway20140930$"

#drop all rules
iptables -F

#set INPUT FORWARD OUTPUT default
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT ACCEPT

#allow enp4s0 forward in
iptables -A FORWARD -i enp4s0 -j ACCEPT

#allow ssh https port
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT

#only allow enp5s0 input accept 3306 for DB backup
bkup_server_ip=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT value FROM $MYSQL_DB.config WHERE name='backup_server_address'"`
iptables -A INPUT -i enp5s0 -p tcp -s $bkup_server_ip --dport 3306 -j ACCEPT
iptables -A INPUT -i enp5s0 -p tcp -s $bkup_server_ip --sport 22 -j ACCEPT
#only allow enp5s0 input accept 123 for date sync
iptables -A INPUT -i enp5s0 -p udp --sport 123 -j ACCEPT

# ------IPv6------
#drop all rules
ip6tables -F

#set INPUT FORWARD OUTPUT default
ip6tables -P INPUT DROP
ip6tables -P FORWARD DROP
ip6tables -P OUTPUT ACCEPT

#allow ipv6-icmp
ip6tables -A INPUT -p ipv6-icmp -j ACCEPT
ip6tables -A FORWARD -p ipv6-icmp -j ACCEPT

#allow enp4s0 forward in
ip6tables -A FORWARD -i enp4s0 -j ACCEPT

#allow ssh https port
ip6tables -A INPUT -p tcp --dport 22 -j ACCEPT
ip6tables -A INPUT -p tcp --dport 443 -j ACCEPT

set +x

