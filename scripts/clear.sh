#!/bin/sh
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -F
ip6tables -P INPUT ACCEPT
ip6tables -P OUTPUT ACCEPT
ip6tables -P FORWARD ACCEPT
ip6tables -F

#kill all filter and gateway processes
pid="0"
ps -ef | grep modbus | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
ps -ef | grep mbusd | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
ps -ef | grep dlt_gw | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
ps -ef | grep cjt_gw | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
ps -ef | grep gdw_filter | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
ps -ef | grep bacnet_filter | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
ps -ef | grep dnp_filter | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
ps -ef | grep ethernet_filter | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
ps -ef | grep 99monitor.sh | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
ps -ef | grep dos_detector | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
