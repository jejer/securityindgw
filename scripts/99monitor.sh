#!/bin/sh

set -x

MYSQL_DB="gateway"
MYSQL_USER="gateway"
MYSQL_PASS="gateway20140930$"

per=""
harddisk_threshold=""
harddisk_delete_threshold=""
while [ "1" = "1" ]
do
	harddisk_threshold=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT value FROM $MYSQL_DB.config WHERE name='harddisk_threshold'"`
	harddisk_delete_threshold=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT value FROM $MYSQL_DB.config WHERE name='harddisk_delete_threshold'"`
	per=`df -h|grep /dev/sda1|awk '{print $5}'`
	per=${per%\%}
	
	if [ "$per" -gt "$harddisk_delete_threshold" ]; then
		#report
		mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "INSERT INTO $MYSQL_DB.log(created_time,operate_type,level,operate_detail) VALUES(NOW(),51,1,'DISK USAGE > $harddisk_delete_threshold %, DELETE OLDEST 100 AUDIT_LOG.')"
		#delete records
		mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "DELETE FROM $MYSQL_DB.audit_log ORDER BY id LIMIT 100"
	fi
	if [ "$per" -gt "$harddisk_threshold" ]; then
		#report 
		mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "INSERT INTO $MYSQL_DB.log(created_time,operate_type,level,operate_detail) VALUES(NOW(),51,2,'DISK USAGE > $harddisk_threshold %')"
	fi
	
	if [ ! -f /root/jejer/mbusd ]; then
		mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "INSERT INTO $MYSQL_DB.log(created_time,operate_type,level,operate_detail) VALUES(NOW(),52,2,'mbusd file lost.')"
	fi
	if [ ! -f /root/jejer/modbus_filter ]; then
		mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "INSERT INTO $MYSQL_DB.log(created_time,operate_type,level,operate_detail) VALUES(NOW(),52,2,'modbus_filter file lost.')"
	fi
	if [ ! -f /root/jejer/dlt_gw ]; then
		mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "INSERT INTO $MYSQL_DB.log(created_time,operate_type,level,operate_detail) VALUES(NOW(),52,2,'dlt_gw file lost.')"
	fi
	if [ ! -f /root/jejer/cjt_gw ]; then
		mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "INSERT INTO $MYSQL_DB.log(created_time,operate_type,level,operate_detail) VALUES(NOW(),52,2,'cjt_gw file lost.')"
	fi
	if [ ! -f /root/jejer/gdw_filter ]; then
		mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "INSERT INTO $MYSQL_DB.log(created_time,operate_type,level,operate_detail) VALUES(NOW(),52,2,'gdw_filter file lost.')"
	fi
	if [ ! -f /root/jejer/bacnet_filter ]; then
		mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "INSERT INTO $MYSQL_DB.log(created_time,operate_type,level,operate_detail) VALUES(NOW(),52,2,'bacnet_filter file lost.')"
	fi
	if [ ! -f /root/jejer/dnp_filter ]; then
		mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "INSERT INTO $MYSQL_DB.log(created_time,operate_type,level,operate_detail) VALUES(NOW(),52,2,'dnp_filter file lost.')"
	fi
	if [ ! -f /root/jejer/ethernet_filter ]; then
		mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "INSERT INTO $MYSQL_DB.log(created_time,operate_type,level,operate_detail) VALUES(NOW(),52,2,'ethernet_filter file lost.')"
	fi
	if [ ! -f /root/jejer/dos_detector ]; then
		mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "INSERT INTO $MYSQL_DB.log(created_time,operate_type,level,operate_detail) VALUES(NOW(),52,2,'dos_detector file lost.')"
	fi
	
	ntpdate `mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT value FROM $MYSQL_DB.config WHERE name='timesync_address'"`
	
	sleep 5m
done

set +x


