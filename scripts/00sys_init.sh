#!/bin/sh

set -x

MYSQL_DB="gateway"
MYSQL_USER="gateway"
MYSQL_PASS="gateway20140930$"

#kill all filter and gateway processes
pid="0"
ps -ef | grep modbus | grep -v "grep" | while read -r line
do
	pid=`echo "$line"|awk '{print $2}'`
	kill -9 $pid
done
ps -ef | grep mbusd | grep -v "grep" | while read -r line
do
	pid=`echo "$line"|awk '{print $2}'`
	kill -9 $pid
done
ps -ef | grep dlt_gw | grep -v "grep" | while read -r line
do
	pid=`echo "$line"|awk '{print $2}'`
	kill -9 $pid
done
ps -ef | grep cjt_gw | grep -v "grep" | while read -r line
do
	pid=`echo "$line"|awk '{print $2}'`
	kill -9 $pid
done
ps -ef | grep gdw_filter | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
ps -ef | grep bacnet_filter | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
ps -ef | grep dnp_filter | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
ps -ef | grep ethernet_filter | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done
ps -ef | grep dos_detector | grep -v "grep" | while read -r line
do
        pid=`echo "$line"|awk '{print $2}'`
        kill -9 $pid
done

#loading serial port expansion card driver
modprobe mxser

#DNS config
grep "4.2.2.1" /etc/resolv.conf
if [ "$?" = "1" ]; then
	echo "nameserver 4.2.2.1" >> /etc/resolv.conf
fi

#Clean IP address
ifconfig enp3s0 up
ifconfig enp4s0 up
ifconfig enp5s0 up
ip addr flush dev enp3s0
ip addr flush dev enp4s0
ip addr flush dev enp5s0

#Set fix IP on enp5s0 for DB backup and Time Sync
ifconfig enp5s0 192.168.254.1 netmask 255.255.255.0

#Set IP address
enp3s0ipv4=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv4 FROM $MYSQL_DB.ethernet_port_config WHERE id='1'"`
enp3s0ipv4msk=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv4_mask FROM $MYSQL_DB.ethernet_port_config WHERE id='1'"`
enp3s0ipv6=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv6 FROM $MYSQL_DB.ethernet_port_config WHERE id='1'"`
enp3s0ipv6msk=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv6_mask FROM $MYSQL_DB.ethernet_port_config WHERE id='1'"`

enp4s0ipv4=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv4 FROM $MYSQL_DB.ethernet_port_config WHERE id='2'"`
enp4s0ipv4msk=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv4_mask FROM $MYSQL_DB.ethernet_port_config WHERE id='2'"`
enp4s0ipv6=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv6 FROM $MYSQL_DB.ethernet_port_config WHERE id='2'"`
enp4s0ipv6msk=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv6_mask FROM $MYSQL_DB.ethernet_port_config WHERE id='2'"`

if [ -n "$enp3s0ipv4" ]; then
	ifconfig enp3s0 $enp3s0ipv4 netmask $enp3s0ipv4msk
fi
if [ -n "$enp3s0ipv6" ]; then
	ifconfig enp3s0 inet6 add $enp3s0ipv6/$enp3s0ipv6msk
fi

if [ -n "$enp4s0ipv4" ]; then
	ifconfig enp4s0 $enp4s0ipv4 netmask $enp4s0ipv4msk
fi
if [ -n "$enp4s0ipv6" ]; then
	ifconfig enp4s0 inet6 add $enp4s0ipv6/$enp4s0ipv6msk
fi

#Set serial ports
stty -F /dev/ttyM0 -parenb -parodd -cmspar cs8 hupcl -cstopb cread clocal -crtscts
stty -F /dev/ttyM0 -ignbrk -brkint -ignpar -parmrk -inpck -istrip -inlcr -igncr -icrnl -ixon -ixoff -iuclc -ixany -imaxbel -iutf8
stty -F /dev/ttyM0 -opost -olcuc -ocrnl onlcr -onocr -onlret -ofill -ofdel nl0 cr0 tab0 bs0 vt0 ff0
stty -F /dev/ttyM0 -isig -icanon -iexten -echo echoe echok -echonl -noflsh -xcase -tostop -echoprt echoctl echoke
stty -F /dev/ttyM1 -parenb -parodd -cmspar cs8 hupcl -cstopb cread clocal -crtscts
stty -F /dev/ttyM1 -ignbrk -brkint -ignpar -parmrk -inpck -istrip -inlcr -igncr -icrnl -ixon -ixoff -iuclc -ixany -imaxbel -iutf8
stty -F /dev/ttyM1 -opost -olcuc -ocrnl onlcr -onocr -onlret -ofill -ofdel nl0 cr0 tab0 bs0 vt0 ff0
stty -F /dev/ttyM1 -isig -icanon -iexten -echo echoe echok -echonl -noflsh -xcase -tostop -echoprt echoctl echoke
stty -F /dev/ttyM2 -parenb -parodd -cmspar cs8 hupcl -cstopb cread clocal -crtscts
stty -F /dev/ttyM2 -ignbrk -brkint -ignpar -parmrk -inpck -istrip -inlcr -igncr -icrnl -ixon -ixoff -iuclc -ixany -imaxbel -iutf8
stty -F /dev/ttyM2 -opost -olcuc -ocrnl onlcr -onocr -onlret -ofill -ofdel nl0 cr0 tab0 bs0 vt0 ff0
stty -F /dev/ttyM2 -isig -icanon -iexten -echo echoe echok -echonl -noflsh -xcase -tostop -echoprt echoctl echoke
stty -F /dev/ttyM3 -parenb -parodd -cmspar cs8 hupcl -cstopb cread clocal -crtscts
stty -F /dev/ttyM3 -ignbrk -brkint -ignpar -parmrk -inpck -istrip -inlcr -igncr -icrnl -ixon -ixoff -iuclc -ixany -imaxbel -iutf8
stty -F /dev/ttyM3 -opost -olcuc -ocrnl onlcr -onocr -onlret -ofill -ofdel nl0 cr0 tab0 bs0 vt0 ff0
stty -F /dev/ttyM3 -isig -icanon -iexten -echo echoe echok -echonl -noflsh -xcase -tostop -echoprt echoctl echoke

id="1"
cs="8"
rate="0"
stopb="1"
verify="0"
fc="0"
mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT uid,baud_rate,data_bit,stop_bit,verify,flow_control FROM $MYSQL_DB.serial_port_config" | while read -r line
do 
	id=`echo "$line"|awk '{print $1}'`
	rate=`echo "$line"|awk '{print $2}'`
	cs=`echo "$line"|awk '{print $3}'`
	stopb=`echo "$line"|awk '{print $4}'`
	verify=`echo "$line"|awk '{print $5}'`
	fc=`echo "$line"|awk '{print $6}'`
	
	if [ "$stopb"x = "2"x ]; then
		stopb="cstopb"
	else
		stopb="-cstopb"
	fi
	
	if [ "$verify"x = "n"x ]; then
		verify="-parenb"
	fi
	if [ "$verify"x = "e"x ]; then
		verify="parenb -parodd"
	fi
	if [ "$verify"x = "o"x ]; then
		verify="parenb parodd"
	fi
	
	if [ "$fc"x != "1"x ]; then
		fc="-crtscts"
	else
		fc="crtscts"
	fi
	
	stty -F /dev/ttyM$id $rate cs$cs $stopb $verify $fc
			
done

#config sshd idle timeout interval
old_timeout=`grep ClientAliveInterval /etc/ssh/sshd_config|grep -v '#'|awk '{print $2}'`
new_timeout=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT value FROM $MYSQL_DB.config WHERE name='ssh_time'"`
new_timeout=$[new_timeout*60]
if [ "$new_timeout"x != ""x ]; then
	if [ "$old_timeout"x != ""x ]; then
		sed -i "s/^ClientAliveInterval.*/ClientAliveInterval $new_timeout/g" /etc/ssh/sshd_config
	else
		echo "ClientAliveInterval $new_timeout" >> /etc/ssh/sshd_config
	fi
fi
if [ "`grep ClientAliveCountMax /etc/ssh/sshd_config|grep -v '#'|awk '{print $2}'`"x != "0"x ]; then
	echo "ClientAliveCountMax 0" >> /etc/ssh/sshd_config
fi
systemctl restart sshd

#enable ip forwarding
echo "1" > /proc/sys/net/ipv4/ip_forward
echo "1" > /proc/sys/net/ipv6/conf/all/forwarding

set +x
