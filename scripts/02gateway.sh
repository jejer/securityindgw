#!/bin/sh

set -x

MYSQL_DB="gateway"
MYSQL_USER="gateway"
MYSQL_PASS="gateway20140930$"

#devices
uid="0"
type=""
protocol=""
ipv4=""
ipv6=""
upipv4=""
upipv6=""
speed=""
l3protocol=""
port=""
mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT uid FROM $MYSQL_DB.industry_device" | while read -r line
do 
	uid=$line
	protocol=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT industry_protocol FROM $MYSQL_DB.industry_device WHERE uid=$uid"`
	
	if [ "$uid" -lt "99" ]; then
		upipv4=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv4_upper FROM $MYSQL_DB.industry_device WHERE uid=$uid"`
		upipv6=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv6_upper FROM $MYSQL_DB.industry_device WHERE uid=$uid"`
		
		if [ "$protocol"x = "MODBUSRTU"x ]; then
			iptables -A INPUT -i enp3s0 -s $upipv4 -p tcp --dport $(($uid+5000)) -j NFQUEUE --queue-num $(($uid+5000))
			speed=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT baud_rate FROM $MYSQL_DB.serial_port_config WHERE uid=$uid"`
			/root/jejer/mbusd -p /dev/ttyM$uid -s $speed -P $(($uid+5000)) -v 9 -N 2 -W 100 -R 500
			/root/jejer/modbus_filter $(($uid+5000)) &
		fi
		if [ "$protocol"x = "DLT"x ]; then
			iptables -A INPUT -i enp3s0 -s $upipv4 -p udp --dport $(($uid+5000)) -j ACCEPT
			/root/jejer/dlt_gw -d /dev/ttyM$uid -p $(($uid+5000)) &
		fi
		if [ "$protocol"x = "CJT"x ]; then
			iptables -A INPUT -i enp3s0 -s $upipv4 -p udp --dport $(($uid+5000)) -j ACCEPT
			/root/jejer/cjt_gw -d /dev/ttyM$uid -p $(($uid+5000)) &
		fi
	else
		type=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT type FROM $MYSQL_DB.industry_device WHERE uid=$uid"`
		ipv4=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv4 FROM $MYSQL_DB.industry_device WHERE uid=$uid"`
		ipv6=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv6 FROM $MYSQL_DB.industry_device WHERE uid=$uid"`
		upipv4=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv4_upper FROM $MYSQL_DB.industry_device WHERE uid=$uid"`
		upipv6=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT ipv6_upper FROM $MYSQL_DB.industry_device WHERE uid=$uid"`
		l3protocol=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT transmission_protocol FROM $MYSQL_DB.industry_device WHERE uid=$uid"`
		port=`mysql -u$MYSQL_USER -p$MYSQL_PASS -s -N -e "SELECT port FROM $MYSQL_DB.industry_device WHERE uid=$uid"`
		if [ "$protocol"x = "TCPIP"x ]; then
			if [ "$type"x = "IP"x ]; then
				#iptables -A FORWARD -i enp3s0 -p tcp --tcp-flags SYN,ACK,FIN,RST SYN -s 192.168.1.77 -d 192.168.2.77 -j NFQUEUE --queue-num $(($uid+5000))
				#/root/jejer/dos_detector $(($uid+5000)) &
				#iptables -A FORWARD -i enp3s0 -p ICMP --icmp-type 8 -s 192.168.1.77 -d 192.168.2.77 -j NFQUEUE --queue-num $(($uid+6000))
				#/root/jejer/dos_detector $(($uid+6000)) &
				iptables -A FORWARD -i enp3s0 -p tcp --tcp-flags SYN,ACK,FIN,RST SYN -s $upipv4 -d $ipv4 -j NFQUEUE --queue-num $(($uid+5000))
				/root/jejer/dos_detector $(($uid+5000)) &
				iptables -A FORWARD -i enp3s0 -p ICMP --icmp-type 8 -s $upipv4 -d $ipv4 -j NFQUEUE --queue-num $(($uid+6000))
				/root/jejer/dos_detector $(($uid+6000)) &
				iptables -A FORWARD -i enp3s0 -d $ipv4 -j ACCEPT
			fi
			if [ "$type"x = "IPv6"x ]; then
				ip6tables -A FORWARD -i enp3s0 -d $ipv6 -j ACCEPT
			fi
		fi
		if [ "$protocol"x = "OPC"x ]; then
			if [ "$type"x = "IP"x ]; then
				iptables -A FORWARD -i enp3s0 -p tcp -s $upipv4 -d $ipv4 --dport 138 -j ACCEPT
			fi
			if [ "$type"x = "IPv6"x ]; then
				ip6tables -A FORWARD -i enp3s0 -p tcp -s $upipv6 -d $ipv6 --dport 138 -j ACCEPT
			fi
		fi
		if [ "$protocol"x = "MODBUSTCP"x ]; then
			if [ "$type"x = "IP"x ]; then
				iptables -A FORWARD -i enp3s0 -p tcp -s $upipv4 -d $ipv4 --dport 502 -j NFQUEUE --queue-num $(($uid+5000))
				/root/jejer/modbus_filter $(($uid+5000)) &
			fi
			if [ "$type"x = "IPv6"x ]; then
				ip6tables -A FORWARD -i enp3s0 -p tcp -s $upipv6 -d $ipv6 --dport 502 -j NFQUEUE --queue-num $(($uid+5000))
				/root/jejer/modbus_filter $(($uid+5000)) &
			fi
		fi
		if [ "$protocol"x = "GDW376.1"x ]; then
			if [ "$type"x = "IP"x ]; then
				iptables -A FORWARD -i enp3s0 -p $l3protocol -s $upipv4 -d $ipv4 --dport $port -j NFQUEUE --queue-num $(($uid+5000))
				/root/jejer/gdw_filter $(($uid+5000)) &
			fi
			if [ "$type"x = "IPv6"x ]; then
				ip6tables -A FORWARD -i enp3s0 -p $l3protocol -s $upipv6 -d $ipv6 --dport $port -j NFQUEUE --queue-num $(($uid+5000))
				/root/jejer/gdw_filter $(($uid+5000)) &
			fi
		fi
		if [ "$protocol"x = "BACNET_IP"x ]; then
			if [ "$type"x = "IP"x ]; then
				iptables -A FORWARD -i enp3s0 -p $l3protocol -s $upipv4 -d $ipv4 --dport $port -j NFQUEUE --queue-num $(($uid+5000))
				/root/jejer/bacnet_filter $(($uid+5000)) &
			fi
			if [ "$type"x = "IPv6"x ]; then
				ip6tables -A FORWARD -i enp3s0 -p $l3protocol -s $upipv6 -d $ipv6 --dport $port -j NFQUEUE --queue-num $(($uid+5000))
				/root/jejer/bacnet_filter $(($uid+5000)) &
			fi
		fi
		if [ "$protocol"x = "DNP3"x ]; then
			if [ "$type"x = "IP"x ]; then
				iptables -A FORWARD -i enp3s0 -p $l3protocol -s $upipv4 -d $ipv4 --dport $port -j NFQUEUE --queue-num $(($uid+5000))
				/root/jejer/dnp_filter $(($uid+5000)) &
			fi
			if [ "$type"x = "IPv6"x ]; then
				ip6tables -A FORWARD -i enp3s0 -p $l3protocol -s $upipv6 -d $ipv6 --dport $port -j NFQUEUE --queue-num $(($uid+5000))
				/root/jejer/dnp_filter $(($uid+5000)) &
			fi
		fi
		if [ "$protocol"x = "ETHERNET_IP"x ]; then
			if [ "$type"x = "IP"x ]; then
				iptables -A FORWARD -i enp3s0 -p $l3protocol -s $upipv4 -d $ipv4 --dport $port -j NFQUEUE --queue-num $(($uid+5000))
				/root/jejer/ethernet_filter $(($uid+5000)) &
			fi
			if [ "$type"x = "IPv6"x ]; then
				ip6tables -A FORWARD -i enp3s0 -p $l3protocol -s $upipv6 -d $ipv6 --dport $port -j NFQUEUE --queue-num $(($uid+5000))
				/root/jejer/ethernet_filter $(($uid+5000)) &
			fi
		fi
	fi
done

set +x
