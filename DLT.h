#ifndef DLT_H_INCLUDED
#define DLT_H_INCLUDED

#pragma pack(push, 1)
struct DLT_645 {
    unsigned char start1; /* 0x68 */
    unsigned char addr[6];
    unsigned char start2; /* 0x68 */
    unsigned char control_code;
    unsigned char data_len;
    unsigned char data[0];
};

struct DLT_645_WITH_DI {
    struct DLT_645 hdr;
    unsigned char DI0;
    unsigned char DI1;
    unsigned char DI2;
    unsigned char DI3;
};
#pragma pack(pop)
//  unsigned char crc;
//  unsigned char end;

#define DLT_FRAME_HEAD 0xFE
#define DLT_START      0x68
#define DLT_END        0x16

#endif // DLT_H_INCLUDED
