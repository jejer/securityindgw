#ifndef _BACNET_IP_FILTER_H
#define _BACNET_IP_FILTER_H

typedef enum {
    BACNET_IP_FILTER_CONFIRMED_SERVICE = 0,     
    BACNET_IP_FILTER_UNCONFIRMED_SERVICE = 1
} BACNetIPFilterModeType;

typedef struct BACNetIPFilterType_st
{	
    //第一级，服务过滤
    unsigned  char  ServiceType;//0:有证实的服务过滤，1：无证实的服务过滤
    unsigned  char	ServiceNum_Start;
    unsigned  char	ServiceNum_End;

    //第二级，object过滤
    unsigned  char	ObjectEnable;//是否启用，在第一级为有证实的服务过滤且服务号属于：06、07、08、09、12、14、15、16的前提下
    unsigned  short	ObjectNum_Start; 
    unsigned  short ObjectNum_End; 

    //第三级，instance过滤
    unsigned  char	InstanceEnable;//是否启用，在第二级启用的前提下
    unsigned  int 	InstanceNum_Start; 
    unsigned  int	InstanceNum_End;
} BACNetIPFilterType;

/***************************************************************************************************
*\Function      BACnetIPFilterInit
*\Description   BACNet/IP协议过滤初始化
*\Parameter     filter_list 协议过滤规则列表，类型为BACNetIPFilterType类型的数组
*\Parameter     num         协议过滤规则数目
*\Return        int         是否初始化成功，0：成功，1：失败
*\Note          
***************************************************************************************************/
int BACnetIPFilterInit(BACNetIPFilterType * filter_list, int num);

/***************************************************************************************************
*\Function      BACnetIPFilterCheck
*\Description   BACNet/IP协议数据包过滤检查
*\Parameter     ip_packet_buf           IP层数据包
*\Parameter     ip_packet_buf_length    IP数据包长度
*\Return        int                     该数据包是否可通过,0:通过，1：不能通过
*\Note          
***************************************************************************************************/
int BACnetIPFilterCheck(char* ip_packet_buf, int ip_packet_buf_length);

#endif /*_BACNET_IP_FILTER_H*/
