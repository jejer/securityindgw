#ifndef CJT_H_INCLUDED
#define CJT_H_INCLUDED

#pragma pack(push, 1)
struct CJT_188 {
    unsigned char start; /* 0x68 */
    unsigned char device_type;
    unsigned char addr[7];
    unsigned char control_code;
    unsigned char data_len;
    unsigned char data[0];
};

struct CJT_188_WITH_DI {
    struct CJT_188 hdr;
    unsigned char DI0;
    unsigned char DI1;
};
#pragma pack(pop)
//  unsigned char crc;
//  unsigned char end;

#define CJT_FRAME_HEAD 0xFE
#define CJT_START      0x68
#define CJT_END        0x16

#endif // CJT_H_INCLUDED
