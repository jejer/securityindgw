#include "dnp_3_0_filter.h"
#include <stdlib.h>
#include <string.h>

typedef unsigned int                u32;    /*!<u32*/
typedef unsigned short              u16;    /*!<u16*/
typedef short                       s16;    /*!<u16*/
typedef unsigned char               u8;     /*!<u8*/
typedef enum
{
    FALSE = 0,      /*!<FALSE，假*/
    TRUE = !FALSE   /*!<TRUE，真*/
}bool;

#pragma pack(1)
typedef struct FT3FrameHeadType_st
{
    u16 StartChar;      /*起始字符*/
    u8  Length;         /*长度*/
    u8  Control;        /*控制字*/
    u16 Destination;
    u16 Source;
    u16 Crc;            
}FT3FrameHeadType;

typedef struct FT3FrameType_st
{
    struct 
    {
        u8 Control;
    }TransportLayer;
    struct
    {
        u8 Control;
        u8 FunCode;
    }ApplicationLayerHeadType;
    struct
    {
        u8 ObjectGroup;
        u8 ObjectVariation;
    }ObjectField;
}FT3FrameType;
#pragma pack()

static DNP3FilterType* DNPFilterList = NULL;
static int DNPFilterCount = 0;

#define MyMallloc   malloc
#define MyFree      free
#define MyMemCpy    memcpy

#ifndef WIN32
// #define MySemStruct             struct semaphore
// #define MySemInit(sem_ptr)      init_MUTEX_LOCKED(sem_ptr)
// #define MySemWait(sem_ptr)      down(sem_ptr)
// #define MySemPost(sem_ptr)      up(sem_ptr)

#include<semaphore.h>
#define MySemStruct             sem_t
#define MySemInit(sem_ptr)      sem_init(sem_ptr, 0, 1)
#define MySemWait(sem_ptr)      sem_wait(sem_ptr)
#define MySemPost(sem_ptr)      sem_post(sem_ptr)
#else

#define MySemStruct         int
#define MySemInit(sem_ptr)
#define MySemWait(sem_ptr)
#define MySemPost(sem_ptr)

#endif // WIN32


static MySemStruct FilterListSem;

/***************************************************************************************************
*\Function      FilterMatch
*\Description   遍历过滤规则,判断合法性
*\Parameter     afn     待检查的AFN码
*\Return        bool    是否合法
*\Note          
***************************************************************************************************/
static bool FilterMatch(u8 afn, u8 object_group, u8 object_variation)
{
    int i = 0;

    if (DNPFilterCount == 0 || DNPFilterList == NULL)
    {
        return FALSE;
    }

    /***************************************************************************************************
    *互斥
    ***************************************************************************************************/
    MySemWait(&FilterListSem);

    for (i = DNPFilterCount - 1; i >= 0; i--)
    {
        /*判断AFN*/
        if ((afn >= DNPFilterList[i].AFN_Start) && (afn <= DNPFilterList[i].AFN_End))
        {
            if (afn == 0x03 || afn == 0x04 || afn == 0x05 || afn == 0x06)
            {
                /*判断是否开启ObjectGroup过滤*/
                if (DNPFilterList[i].ObjectGroupEnable == 1)
                {
                    /*判断ObjectGroup是否合法*/
                    if ((object_group >= DNPFilterList[i].ObjectGroupNum_Start) && (object_group <= DNPFilterList[i].ObjectGroupNum_End))
                    {
                        /*判断ObjectVariation过滤是否开启*/
                        if (DNPFilterList[i].ObjectVariationEnable == 1)
                        {
                            /*判断ObjectVariation是否合法*/
                            if (!((object_variation >= DNPFilterList[i].ObjectVariationNum_Start) && (object_variation <= DNPFilterList[i].ObjectVariationNum_End)))
                            {   
                                /*ObjectVariation不合法*/
                                continue;
                            }
                        }
                    }
                    else
                    {
                        /*ObjectGroup不合法*/
                        continue;
                    }
                }
            }

            MySemPost(&FilterListSem);
            return TRUE;
        }
    }

    /***************************************************************************************************
    *互斥结束
    ***************************************************************************************************/
    MySemPost(&FilterListSem);

    return FALSE;
}

/***************************************************************************************************
*\Function      GetDNPProtocolContent
*\Description   从IP包中获取dnp协议内容
*\Parameter     ip      IP包
*\Parameter     ip_len  IP包长度
*\Parameter     dnp     获取后的dnp包
*\Parameter     length  dnp包长度
*\Return        bool
*\Note          
***************************************************************************************************/
static bool GetDNPProtocolContent(u8* ip, u32 ip_len, u8** dnp, u32* length)
{
    /*先去掉IP头*/
    u32 len = (ip[0] & 0x0f) * 4;   /*计算首部长度*/
    u8* tcpudp = (u8*)(ip + len);
    u8 protocol = ip[9];
    len = (ip[2])*256 + ip[3] - len;

    /*再去掉TCP头或UDP头*/
    if (protocol == 6)
    {
        *dnp = tcpudp + (tcpudp[12]>>4) * 4;
        *length = len - (tcpudp[12]>>4) * 4;
    }
    else if (protocol == 17)
    {
        *dnp = tcpudp + 8;
        *length = (tcpudp[4]*256+tcpudp[5] - 8);
    }
    else
    {
        return FALSE;
    }

    return TRUE;
}

/***************************************************************************************************
*\Function      ComputeCRC
*\Description   计算CRC
*\Parameter     dataOctet
*\Parameter     crcAccum
*\Return        void
*\Note          
***************************************************************************************************/
static void ComputeCRC(u8 dataOctet, u16 *crcAccum) 
{ 
    static unsigned short crcLookUpTable[256] = // Look up table values 
    { 
        0x0000, 0x365E, 0x6CBC, 0x5AE2, 0xD978, 0xEF26, 0xB5C4, 0x839A, 
        0xFF89, 0xC9D7, 0x9335, 0xA56B, 0x26F1, 0x10AF, 0x4A4D, 0x7C13, 
        0xB26B, 0x8435, 0xDED7, 0xE889, 0x6B13, 0x5D4D, 0x07AF, 0x31F1, 
        0x4DE2, 0x7BBC, 0x215E, 0x1700, 0x949A, 0xA2C4, 0xF826, 0xCE78, 
        0x29AF, 0x1FF1, 0x4513, 0x734D, 0xF0D7, 0xC689, 0x9C6B, 0xAA35, 
        0xD626, 0xE078, 0xBA9A, 0x8CC4, 0x0F5E, 0x3900, 0x63E2, 0x55BC, 
        0x9BC4, 0xAD9A, 0xF778, 0xC126, 0x42BC, 0x74E2, 0x2E00, 0x185E, 
        0x644D, 0x5213, 0x08F1, 0x3EAF, 0xBD35, 0x8B6B, 0xD189, 0xE7D7, 
        0x535E, 0x6500, 0x3FE2, 0x09BC, 0x8A26, 0xBC78, 0xE69A, 0xD0C4, 
        0xACD7, 0x9A89, 0xC06B, 0xF635, 0x75AF, 0x43F1, 0x1913, 0x2F4D, 
        0xE135, 0xD76B, 0x8D89, 0xBBD7, 0x384D, 0x0E13, 0x54F1, 0x62AF, 
        0x1EBC, 0x28E2, 0x7200, 0x445E, 0xC7C4, 0xF19A, 0xAB78, 0x9D26, 
        0x7AF1, 0x4CAF, 0x164D, 0x2013, 0xA389, 0x95D7, 0xCF35, 0xF96B, 
        0x8578, 0xB326, 0xE9C4, 0xDF9A, 0x5C00, 0x6A5E, 0x30BC, 0x06E2, 
        0xC89A, 0xFEC4, 0xA426, 0x9278, 0x11E2, 0x27BC, 0x7D5E, 0x4B00, 
        0x3713, 0x014D, 0x5BAF, 0x6DF1, 0xEE6B, 0xD835, 0x82D7, 0xB489, 
        0xA6BC, 0x90E2, 0xCA00, 0xFC5E, 0x7FC4, 0x499A, 0x1378, 0x2526, 
        0x5935, 0x6F6B, 0x3589, 0x03D7, 0x804D, 0xB613, 0xECF1, 0xDAAF, 
        0x14D7, 0x2289, 0x786B, 0x4E35, 0xCDAF, 0xFBF1, 0xA113, 0x974D, 
        0xEB5E, 0xDD00, 0x87E2, 0xB1BC, 0x3226, 0x0478, 0x5E9A, 0x68C4, 
        0x8F13, 0xB94D, 0xE3AF, 0xD5F1, 0x566B, 0x6035, 0x3AD7, 0x0C89, 
        0x709A, 0x46C4, 0x1C26, 0x2A78, 0xA9E2, 0x9FBC, 0xC55E, 0xF300, 
        0x3D78, 0x0B26, 0x51C4, 0x679A, 0xE400, 0xD25E, 0x88BC, 0xBEE2, 
        0xC2F1, 0xF4AF, 0xAE4D, 0x9813, 0x1B89, 0x2DD7, 0x7735, 0x416B, 
        0xF5E2, 0xC3BC, 0x995E, 0xAF00, 0x2C9A, 0x1AC4, 0x4026, 0x7678, 
        0x0A6B, 0x3C35, 0x66D7, 0x5089, 0xD313, 0xE54D, 0xBFAF, 0x89F1, 
        0x4789, 0x71D7, 0x2B35, 0x1D6B, 0x9EF1, 0xA8AF, 0xF24D, 0xC413, 
        0xB800, 0x8E5E, 0xD4BC, 0xE2E2, 0x6178, 0x5726, 0x0DC4, 0x3B9A, 
        0xDC4D, 0xEA13, 0xB0F1, 0x86AF, 0x0535, 0x336B, 0x6989, 0x5FD7, 
        0x23C4, 0x159A, 0x4F78, 0x7926, 0xFABC, 0xCCE2, 0x9600, 0xA05E, 
        0x6E26, 0x5878, 0x029A, 0x34C4, 0xB75E, 0x8100, 0xDBE2, 0xEDBC, 
        0x91AF, 0xA7F1, 0xFD13, 0xCB4D, 0x48D7, 0x7E89, 0x246B, 0x1235 
    }; 
    *crcAccum = (*crcAccum >> 8) ^ crcLookUpTable[(*crcAccum ^ dataOctet) & 0xFF]; 
} /* end ComputeCRC() */ 

/***************************************************************************************************
*\Function      CheckPacketCrc
*\Description   检测包CRC校验是否正确
*\Parameter     buf     包数据
*\Parameter     len     数据长度
*\Return        bool    是否正确
*\Note          
***************************************************************************************************/
static bool CheckPacketCrc(u8* buf, u32 len)
{
    u32 idx; // Index 
    u16 crc = 0; // Initialize 
    for (idx = 0; idx < len; idx++) 
    {
        ComputeCRC(buf[idx],&crc);
    }
    crc = ~crc; // Invert 
    if (buf[idx++] == (unsigned char)crc && \
        buf[idx] == (unsigned char)(crc >> 8)) 
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/***************************************************************************************************
*\Function      DataExtraction
*\Description   数据提取
*\Return        void
*\Note          
***************************************************************************************************/
static u32 DataExtraction(u8* buf, u32 len, u8* data_buf)
{
    u8 data_count = 0;
    u32 i = 0;
    u32 data_len = 0;
    while (i < len - 2)
    {
        data_buf[data_len++] = buf[i];
        data_count++;
        i++;
        if(data_count == 16)
        {
            if(!CheckPacketCrc(&buf[i - 16], 16))
            {
                return 0;
            }
            i += 2;
            data_count = 0;
        }
    }
    if(!CheckPacketCrc(&buf[i - data_count], data_count))
    {
        return 0;
    }
    return data_len;
}

/***************************************************************************************************
*\Function      Dnp3Analysis
*\Description   DNP解析
*\Parameter     buf     待解析数据
*\Parameter     len     待解析数据长度
*\Return        bool
*\Note          
***************************************************************************************************/
static bool Dnp3Analysis(u8* buf, u32 len)
{
    FT3FrameHeadType* head = (FT3FrameHeadType*)buf;
    FT3FrameType* frame_data = NULL;
    u8* data_tmp_buf = NULL;

    /*校验头部数据*/
    if(len < sizeof(FT3FrameHeadType))
    {
        /*数据长度过短*/
        return FALSE;
    }
    if(head->StartChar != 0x6405)
    {
        /*头部起始标志字验证错误*/
        return FALSE;
    }
    if(head->Length < 5)
    {
        /*报文长度最小为5*/
        return FALSE;
    }
    if(!CheckPacketCrc(buf, 8))
    {
        /*验证CRC错误*/
        return FALSE;
    }
    if(head->Length == 5)
    {
        return TRUE;
    }
    data_tmp_buf = (u8*)MyMallloc(head->Length - 5); 
    /*数据转换加校验*/
    if(DataExtraction(buf + 10, len - 10, data_tmp_buf) == 0)
    {
        /*数据提取或校验失败*/
        MyFree(data_tmp_buf);
        return FALSE;
    }

    frame_data = (FT3FrameType*)data_tmp_buf;


    /*根据过滤协议过滤*/
    if (FilterMatch(frame_data->ApplicationLayerHeadType.FunCode, frame_data->ObjectField.ObjectGroup, frame_data->ObjectField.ObjectVariation) == FALSE)
    {
        MyFree(data_tmp_buf);
        return FALSE;
    }

    MyFree(data_tmp_buf);
    return TRUE; 
}

/***************************************************************************************************
*\Function      DNP3FilterInit
*\Description   DNP3.0协议过滤初始化
*\Parameter     filter_list 协议过滤规则列表，类型为DNPFilterAFNType类型的数组
*\Parameter     num         协议过滤规则数目
*\Return        int         是否初始化成功，0：成功，1：失败
*\Note          
***************************************************************************************************/
int DNP3FilterInit(DNP3FilterType* filter_list, int num)
{
    static bool once_run = TRUE;

    if (once_run == TRUE)
    {
        once_run = FALSE;
        MySemInit(&FilterListSem);
    }

    /*安检*/
    if (filter_list == NULL || num == 0)
    {
        return 1;
    }

    /***************************************************************************************************
    *互斥
    ***************************************************************************************************/
    MySemWait(&FilterListSem);

    /*释放旧的规则*/
    if ((DNPFilterList != NULL) && (DNPFilterCount != 0))
    {
        MyFree(DNPFilterList);
    }

    DNPFilterList = (DNP3FilterType*)MyMallloc(sizeof(DNP3FilterType) * num);
    DNPFilterCount = num;

    MyMemCpy(DNPFilterList, filter_list, sizeof(DNP3FilterType) * num);
    /***************************************************************************************************
    *互斥结束
    ***************************************************************************************************/
    MySemPost(&FilterListSem);

    return 0;
}

/***************************************************************************************************
*\Function      DNP3FilterCheck
*\Description   DNP3.0协议数据包过滤检查
*\Parameter     ip_packet_buf           IP层数据包
*\Parameter     ip_packet_buf_length    IP数据包长度
*\Return        int                     该数据包是否可通过,0:通过，1：不能通过
*\Note          
***************************************************************************************************/
int DNP3FilterCheck(char* ip_packet_buf, int ip_packet_buf_length)
{
    u8* dnp = NULL;
    u32 dnp_len = 0;

    if (ip_packet_buf == NULL || ip_packet_buf_length == 0)
    {
        return 1;
    }

    if (GetDNPProtocolContent((u8*)ip_packet_buf, ip_packet_buf_length, &dnp, &dnp_len) == FALSE)
    {
        return 1;
    }

    if (Dnp3Analysis(dnp, dnp_len) == FALSE)
    {
        return 1;
    }

    return 0;
}
